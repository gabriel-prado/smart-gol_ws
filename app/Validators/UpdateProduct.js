'use strict'
const Product = use('App/Models/Product')

class UpdateProduct {
  get rules () {
    return {
      id: 'required|integer',
      name: 'string',
      photo: 'string',
      amount: 'integer',
      points: 'integer',
      description: 'string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = Product.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = UpdateProduct
