'use strict'

class ChangeStatus {
  get rules () {
    return {
      approved: 'required|boolean'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = {
      approved: body.approved
    }

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = ChangeStatus
