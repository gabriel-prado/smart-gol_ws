'use strict'

const Task = use('App/Models/Task')

class UpdateTask {
  get rules () {
    return {
      id: 'required|integer',
      user_id: 'integer',
      client_id: 'integer',
      company_id: 'integer',
      description: 'string',
      points: 'integer',
      date_start: 'date',
      date_finished: 'date',
      date_limit: 'date',
      date_bonus: 'date',
      points_bonus: 'integer',
      approved: 'boolean'
    }
  }

  get data () {
    const body = this.ctx.request.body
    delete body.approved

    this.ctx.request.body = Task.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }

  get sanitizationRules () {
    return {
      date_start: 'to_date',
      date_finished: 'to_date',
      date_limit: 'to_date',
      date_bonus: 'to_date'
    }
  }

}

module.exports = UpdateTask
