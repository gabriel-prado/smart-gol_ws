'use strict'
const Exchange = use('App/Models/Exchange')

class UpdateExchange {
  get rules () {
    return {
      id: 'required|integer',
      user_id: 'required|integer',
      product_id: 'required|integer',
      date_approved: 'date',
      date_exchange: 'date'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = Exchange.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }

  get sanitizationRules () {
    return {
      date_approved: 'to_date',
      date_exchange: 'to_date'
    }
  }
}

module.exports = UpdateExchange
