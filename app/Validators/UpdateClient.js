'use strict'

const Client = use('App/Models/Client')

class UpdateClient {
  get rules () {
    return {
      id: 'required|integer',
      client_name: 'required|string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = Client.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = UpdateClient
