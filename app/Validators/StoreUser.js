'use strict'

const User = use('App/Models/User')

class StoreUser {
  get rules () {
    return {
      username: 'required|string',
      document: 'required|integer|alpha_numeric|unique:users',
      role: 'required|in:BACKOFFICE,USER',
      email: 'required|email|unique:users',
      password: 'string',
      gender: 'required|string',
      phone: 'required|integer',
      birthdate: 'required|date',
      zipcode: 'required|integer|alpha_numeric',
      address: 'required|string',
      number: 'required|integer|alpha_numeric',
      complement: 'string',
      neighborhood: 'required|string',
      city: 'required|string',
      state: 'required|string',
      firstAcess:'boolean',
      status:'boolean',
      isConfirmed: 'boolean',
      photo:'required|string',
      denied_reason: 'string',
      points: 'integer',
      permissions: 'array',
      'permissions.*': `in:USER_SHOW,USER_APPROVE,USER_CREATE,COMPANY_SHOW,COMPANY_CREATE,TASK_SHOW_APPROVE,TASK_SEND,TASK_CREATE,EXCHANGE_SHOW,EXCHANGE_APPROVE,EXCHANGE_CREATE_PRODUCTS`
    }
  }

  get data () {
    const body = this.ctx.request.body
    delete body.status
    delete body.isConfirmed
    delete body.points

    this.ctx.request.body = User.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }

  get sanitizationRules () {
    return {
      birthdate: 'to_date'
    }
  }

}

module.exports = StoreUser
