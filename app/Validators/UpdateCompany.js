'use strict'

const Company = use('App/Models/Company')

class UpdateCompany {
  get rules () {
    return {
      id: 'required|integer',
      trading_name: 'string',
      company_name: 'string',
      document: 'integer|alpha_numeric',
      responsible_name: 'string',
      responsible_phone: 'integer',
      company_phone: 'integer',
      zipcode: 'integer|alpha_numeric',
      address: 'string',
      number: 'integer|alpha_numeric',
      complement: 'string',
      neighborhood: 'string',
      city: 'string',
      state: 'string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    const clients = this.ctx.request.body.clients || []
    this.ctx.request.body = Company.clearParams(body)
    this.ctx.request.body.clients = clients

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = UpdateCompany
