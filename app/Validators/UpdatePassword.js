'use strict'

class ChangePassword {
  get rules () {
    return {
      oldPassword: 'required|string',
      password: 'required|string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = {
      oldPassword: body.oldPassword,
      password: body.password
    }

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = ChangePassword
