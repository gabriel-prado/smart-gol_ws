'use strict'
const Product = use('App/Models/Product')

class StoreProduct {
  get rules () {
    return {
      name: 'required|string',
      photo: 'required|string',
      amount: 'required|integer',
      points: 'required|integer',
      description: 'string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = Product.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = StoreProduct
