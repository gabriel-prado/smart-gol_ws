'use strict'

const Question = use('App/Models/Question')

class UpdateQuestion {
  get rules () {
    return {
      id: 'required|integer',
      question_title: 'string',
      type: 'in:MULTIPLE_CHOICE,TEXT',
      question_group: 'string',
      photos_required: 'boolean',
      photos_amount: 'integer',
      photos_comment_required: 'boolean'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = Question.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = UpdateQuestion
