'use strict'

const Task = use('App/Models/Task')

class StoreTask {
  get rules () {
    return {
      user_id: 'integer',
      client_id: 'required|integer',
      company_id: 'required|integer',
      description: 'required|string',
      points: 'required|integer',
      date_start: 'date',
      date_finished: 'date',
      date_limit: 'required|date',
      date_bonus: 'date',
      points_bonus: 'integer',
      approved: 'boolean'
    }
  }

  get data () {
    const body = this.ctx.request.body
    delete body.approved

    this.ctx.request.body = Task.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }

  get sanitizationRules () {
    return {
      date_start: 'to_date',
      date_finished: 'to_date',
      date_limit: 'to_date',
      date_bonus: 'to_date'
    }
  }

}

module.exports = StoreTask
