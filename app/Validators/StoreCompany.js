'use strict'

const Company = use('App/Models/Company')

class StoreCompany {
  get rules () {
    return {
      trading_name: 'required|string',
      company_name: 'required|string',
      document: 'required|integer|alpha_numeric|unique:companies',
      responsible_name: 'required|string',
      responsible_phone: 'required|integer',
      company_phone: 'required|integer',
      zipcode: 'required|integer|alpha_numeric',
      address: 'required|string',
      number: 'required|integer|alpha_numeric',
      complement: 'string',
      neighborhood: 'required|string',
      city: 'required|string',
      state: 'required|string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    const clients = this.ctx.request.body.clients || []
    this.ctx.request.body = Company.clearParams(body)
    this.ctx.request.body.clients = clients

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = StoreCompany
