'use strict'

const TaskQuestion = use('App/Models/TaskQuestion')

class StoreTaskQuestion {
  get rules () {
    return {
      task_id: 'required|integer',
      question_id: 'required|integer',
      approved: 'boolean',
      answer: 'string',
      photos: 'array'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = TaskQuestion.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }

}

module.exports = StoreTaskQuestion
