'use strict'

class ChangePassword {
  get rules () {
    return {
      email: 'required|string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = {
      email: body.email
    }

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = ChangePassword
