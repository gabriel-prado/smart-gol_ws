'use strict'

const Client = use('App/Models/Client')

class StoreClient {
  get rules () {
    return {
      client_name: 'required|string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = Client.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }
}

module.exports = StoreClient
