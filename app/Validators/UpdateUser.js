'use strict'

const User = use('App/Models/User')

class UpdateUser {
  get rules () {
    return {
      id: 'required|integer',
      username: 'string',
      document: 'integer|alpha_numeric',
      role: 'in:BACKOFFICE,USER',
      email: 'email',
      password: 'string',
      gender: 'string',
      phone: 'integer',
      birthdate: 'date',
      zipcode: 'integer|alpha_numeric',
      address: 'string',
      number: 'integer|alpha_numeric',
      complement: 'string',
      neighborhood: 'string',
      city: 'string',
      state: 'string',
      firstAcess: 'boolean',
      status: 'boolean',
      isConfirmed: 'boolean',
      photo: 'string',
      denied_reason: 'string',
      permissions: 'array',
      points: 'integer',
      'permissions.*': `in:USER_SHOW,USER_APPROVE,USER_CREATE,COMPANY_SHOW,COMPANY_CREATE,TASK_SHOW_APPROVE,TASK_SEND,TASK_CREATE,EXCHANGE_SHOW,EXCHANGE_APPROVE,EXCHANGE_CREATE_PRODUCTS`
    }
  }

  get data () {
    const body = this.ctx.request.body
    delete body.status
    delete body.isConfirmed
    delete body.points

    this.ctx.request.body = User.clearParams(body)

    return Object.assign({}, this.ctx.request.body, {})
  }

  get sanitizationRules () {
    return {
      birthdate: 'to_date'
    }
  }

}

module.exports = UpdateUser
