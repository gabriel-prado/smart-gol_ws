'use strict'

const User = use('App/Models/User')

class ChangeStatusUser {
  get rules () {
    return {
      status: 'required|boolean',
      denied_reason: 'required_when:status,false|string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    this.ctx.request.body = {
      status: body.status,
      denied_reason: body.denied_reason
    }

    return Object.assign({}, this.ctx.request.body, {})
  }

}

module.exports = ChangeStatusUser
