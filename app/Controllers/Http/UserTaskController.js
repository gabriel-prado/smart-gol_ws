'use strict'

const Task = use('App/Models/Task')
const Question = use('App/Models/Question')
const RoleCheckerException = use('App/Exceptions/RoleCheckerException')
const { HttpException } = require('@adonisjs/generic-exceptions')
const moment = use('moment')

class UserTaskController {

  async index ({ request, auth }) {
    const { user_id } = request.params

    if (Number(user_id) !== auth.user.id)
      throw new RoleCheckerException()

    let tasks = await Task.query().where('user_id', user_id).with('client').with('company').with('questions').fetch()

    tasks = tasks.toJSON()

    tasks.forEach(t => {
      t.questions = t.questions.map(q => {
        q.answer = q.pivot
        delete q.pivot
        return q
      })
      t.questions = Question.orderQuestionsByGroup(t.questions)
    })

    return tasks
  }

  async update({ request, response, auth }) {
    const { user_id, id } = request.params

    const task = await Task.findOrFail(id)

    if (!!task.date_finished)
      throw new RoleCheckerException()

    if (!user_id) {
      task.user_id = auth.user.id
      task.date_start = moment().format('YYYY-MM-DD')
    } else {
      const questions = await task.answers().fetch()
      const notFinished = questions.toJSON().some(element => {
        return !element.answer
      })

      if (!questions.size() || notFinished)
        throw new HttpException ('Task not finished.', 500, 'E_TASK_NOT_FINISHED')

      task.date_finished = moment().format('YYYY-MM-DD')
    }

    await task.save()

    return task
  }

}

module.exports = UserTaskController
