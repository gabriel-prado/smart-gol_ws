'use strict'

const Database = use('Database')

class DashboardController {

  async index({ request }) {
    const { client } = request.all()
    let promises = []

    /* usersApproved */
    promises.push(
      Database
        .from('users')
        .where('status', true)
        .count('* as total'))

    /* usersPending */
    promises.push(
      Database
        .from('users')
        .whereNull('status')
        .count('* as total'))

    /* tasksTotal */
    promises.push(
      Database
        .from('tasks')
        .count('* as total'))

    /* tasksFinished */
    promises.push(
      Database
        .from('tasks')
        .whereNotNull('date_finished')
        .count('* as total'))

    /* tasksPending */
    promises.push(
      Database
        .from('tasks')
        .whereNull('approved')
        .count('* as total'))

    /* tasksApproved */
    promises.push(
      Database
        .from('tasks')
        .where('approved', true)
        .count('* as total'))

    /* exchangesPending */
    promises.push(
      Database
        .from('exchanges')
        .whereNull('date_approved')
        .count('* as total'))

    /* exchangesApproved */
    promises.push(
      Database
        .from('exchanges')
        .whereNotNull('date_approved')
        .count('* as total'))

    if (!!client) {
      promises[2] = promises[2].where('client_id', Number(client))
      promises[3] = promises[3].where('client_id', Number(client))
      promises[4] = promises[4].where('client_id', Number(client))
      promises[5] = promises[5].where('client_id', Number(client))
    }

    const result = await Promise.all(promises)

    return {
      usersApproved: result[0][0].total,
      usersPending: result[1][0].total,
      tasksTotal: result[2][0].total,
      tasksFinished: result[3][0].total,
      tasksPending: result[4][0].total,
      tasksApproved: result[5][0].total,
      exchangesPending: result[6][0].total,
      exchangesApproved: result[7][0].total
    }
  }

}

module.exports = DashboardController
