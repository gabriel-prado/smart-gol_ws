'use strict'

const Task = use('App/Models/Task')

class CompanyTaskController {

  async index({ request }) {
    const { company_id } = request.params
    
    return Task
      .query()
      .where('company_id', company_id)
      .with('client')
      .with('user')
      .with('questions')
      .fetch()
  }

}

module.exports = CompanyTaskController
