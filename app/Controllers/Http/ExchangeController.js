'use strict'

const Exchange = use('App/Models/Exchange')
const User = use('App/Models/User')
const RoleCheckerException = use('App/Exceptions/RoleCheckerException')
const Database = use('Database')
const moment = use('moment')
const { HttpException } = require('@adonisjs/generic-exceptions')

class ExchangeController {

  async index({ request, response, auth }) {
    const { user_id } = request.params

    if (!!user_id && auth.user.role !== 'BACKOFFICE' && (auth.user.id != user_id))
      throw new RoleCheckerException()

    return !!user_id ?
      Exchange.query().where('user_id', user_id).with('user').with('product').fetch() :
      Exchange.query().with('user').with('product').fetch()
  }

  async store({ request, response, auth }) {
    const exchange = request.post()

    if(auth.user.role !== 'BACKOFFICE') exchange.user_id = auth.user.id

    return response.status('201').json(await Exchange.create(exchange))
  }

  async show({ request, auth }) {
    const { user_id, id  } = request.params

    if ((auth.user.id != user_id) && (auth.user.role !== 'BACKOFFICE'))
      throw new RoleCheckerException()

    const query = Exchange.query()

    return !!user_id ?
      query
        .where('user_id', user_id)
        .where('product_id', id)
        .with('product')
        .fetch() :
      query
        .where('id', id)
        .with('product')
        .first()
  }

  async update({ request }) {
    const attributes = request.post()
    let exchange = await Exchange.findOrFail(attributes.id)

    exchange.merge(attributes)
    await exchange.save()

    return exchange
  }

  async destroy({ request }) {
    const { id, user_id } = request.params

    const query = Exchange.query()

    const count = await (!!user_id ?
      query
        .where('product_id', id)
        .where('user_id', user_id)
        .delete() :
      query
        .where('id', id)
        .delete()
    )

    if (count < 1) throw new HttpException(
      'Cannot find database row',
      404,
      'E_MISSING_DATABASE_ROW'
    )
  }

  async changeStatus({ request }) {
    const trx = await Database.beginTransaction()
    const { approved } = request.all()
    const { id } = request.params
    const exchange = await Exchange.findOrFail(id)
    const product = await exchange.product().fetch()
    const user = await User.findOrFail(exchange.user_id)

    if (approved) {
      if (exchange.date_approved !== null) throw new HttpException(
        'This exchange has already been approved.',
        500,
        'E_EXCHANGE_DONE'
      )

      if (user.points < product.points) throw new HttpException(
        'User dont have enough points.',
        500,
        'E_NOT_ENOUGH_VALUE'
      )

      if (product.amount < 1) throw new HttpException(
        'This product dont have stock.',
        500,
        'E_OUT_OF_STOCK'
      )

      user.points = user.points - product.points
      product.amount = product.amount - 1
      exchange.date_approved = moment().format('YYYY-MM-DD')

      await user.save(trx)
      await product.save(trx)
      await exchange.save(trx)
    } else {
      exchange.date_approved = null
      await exchange.save(trx)
    }

    trx.commit()
    return exchange
  }

}

module.exports = ExchangeController
