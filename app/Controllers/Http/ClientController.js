'use strict'

const Client = use('App/Models/Client')

class ClientController {

    async index({ response }) {
      return response.status('200').json(await Client.all())
    }

    async store({ request, response }) {
      const client = request.post()

      return response.status('201').json(await Client.create(client))
    }

    async show({ request }) {
      const { id } = request.params

      return Client.findOrFail(id)
    }

    async update({ request }) {
      const attributes = request.post()
      let client = await Client.findOrFail(attributes.id)

      client.merge(attributes)
      await client.save()

      return client
    }

    async destroy({ request }) {
      const { id } = request.params
      const client = await Client.findOrFail(id)

      await client.delete()
    }

}

module.exports = ClientController
