'use strict'

const swaggerJSDoc = use('swagger-jsdoc')

class SwaggerController {

  index({ request, response }) {
    const options = {
      swaggerDefinition: {
        openapi: '3.0.0',
        info: {
          title: 'Smart Gol',
          version: '1.0.0',
          contact: {
            name: "Gabriel Prado",
            email: 'gabriel.prado.profeta@gmail.com'
          }
        },
        components: {
          securitySchemes: {
            Authorization: {
              type: 'http',
              scheme: 'bearer',
              bearerFormat: 'JWT',
            }
          }
        },
        security: [{
          Authorization: []
        }]
      },
      apis: [
        'public/docs/controllers/*.js',
        'public/docs/controllers/*.yml',
        'public/docs/models/*.js',
        'public/docs/models/*.yml'
      ]
    }

    return swaggerJSDoc(options)
  }

}

module.exports = SwaggerController
