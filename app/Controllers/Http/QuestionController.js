'use strict'

const Question = use('App/Models/Question')

class QuestionController {

  async index({ response }) {
    return response.status('200').json(await Question.all())
  }

  async store({ request, response }) {
    const question = request.post()

    return response.status('201').json(await Question.create(question))
  }

  async show({ request }) {
    const { id } = request.params

    return Question.findOrFail(id)
  }

  async update({ request }) {
    const attributes = request.post()
    let question = await Question.findOrFail(attributes.id)

    question.merge(attributes)
    await question.save()

    return question
  }

  async destroy({ request }) {
    const { id } = request.params
    const question = await Question.findOrFail(id)

    await question.delete()
  }

}

module.exports = QuestionController
