'use strict'

const Product = use('App/Models/Product')

class ProductController {

  async index({ response }) {
    return response.status('200').json(await Product.all())
  }

  async store({ request, response }) {
    const product = request.post()

    return response.status('201').json(await Product.create(product))
  }

  async show({ request }) {
    const { id } = request.params

    return Product.findOrFail(id)
  }

  async update({ request }) {
    const attributes = request.post()
    let product = await Product.findOrFail(attributes.id)

    product.merge(attributes)
    await product.save()

    return product
  }

  async destroy({ request }) {
    const { id } = request.params
    const product = await Product.findOrFail(id)

    await product.delete()
  }

}

module.exports = ProductController
