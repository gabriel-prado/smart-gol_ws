'use strict'

const Company = use('App/Models/Company')
const BulkStorageService = use('App/Services/BulkStorage')
const Database = use('Database')

class CompanyController {

  async index({ response }) {
    return Company
      .query()
      .with('clients')
      .fetch()
  }

  async store({ request, response }) {
    const attributes = request.post()
    let clients = attributes.clients || []
    delete attributes.clients
    const trx = await Database.beginTransaction()
    const company = await Company.create(attributes, trx)

    if (clients.length) {
      let ids = clients.map(c => c.id)
      await company.clients().attach(ids, null, trx)
    }
    trx.commit()
    await company.load('clients')
    return response.status('201').json(company)
  }

  async show({ request }) {
    const { id } = request.params

    const company = await Company.findOrFail(id)
    await company.load('clients')
    return company
  }

  async update({ request }) {
    const attributes = request.post()
    let clients = attributes.clients || []
    delete attributes.clients
    let company = await Company.findOrFail(attributes.id)
    const trx = await Database.beginTransaction()

    company.merge(attributes)
    await company.save(trx)

    if (clients.length) {
      let ids = clients.map(c => c.id)
      await company.clients().sync(ids, null, trx)
    }

    trx.commit()
    await company.load('clients')
    return company
  }

  async destroy({ request }) {
    const { id } = request.params
    const company = await Company.findOrFail(id)

    await company.delete()
  }

  async bulkStorage({ request, response }) {
    const file = request.file('file')

    const res = await BulkStorageService.storage(file, Company, 'trading_name', 'bulk-company.xlsx')

    return response.status('200').json(res)
  }

}

module.exports = CompanyController
