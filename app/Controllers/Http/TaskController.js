'use strict'

const Task = use('App/Models/Task')
const Question = use('App/Models/Question')
const { HttpException } = require('@adonisjs/generic-exceptions')

class TaskController {

  async index({ request, response }) {
    const { user, company, city } = request.all()

    let tasks = Task
      .query()
      .with('company')
      .with('questions')
      .with('user')

    if (!!user || !!company) {
      if(!!company) {
        tasks.whereHas('company', builder => {
          builder.where('company_name', 'like', `%${company}%`)
        })
        tasks.orWhereHas('company', builder => {
          builder.where('trading_name', 'like', `%${company}%`)
        })
      }

      if (!!company && !!user) {
        tasks.orWhereHas('user', builder => {
          builder.where('username', 'like', `%${user}%`)
        })
      } else if (!!user) {
        tasks.whereHas('user', builder => {
          builder.where('username', 'like', `%${user}%`)
        })
      }
    }

    if (!!city) {
      tasks.whereHas('company', builder => {
        builder.where('city', 'like', `%${city}%`)
      })
    }

    tasks = await tasks.fetch()
    tasks = tasks.toJSON()

    tasks.forEach(t => {
      if (!!t.questions) {
        t.questions = t.questions.map(q => {
          q.answer = q.pivot
          delete q.pivot
          return q
        })
        t.questions = Question.orderQuestionsByGroup(t.questions)
      }
    })

    return response.status('200').json(tasks)
  }

  async store({ request, response }) {
    const params = request.post()
    const task = await Task.create(params)
    await task.reload()
    return response.status('201').json(task)
  }

  async show({ request }) {
    const { id } = request.params

    let task = await Task
      .query()
      .where('id', id)
      .with('user')
      .with('company')
      .with('client')
      .with('questions', builder => {
        builder.with('answer')
      })
      .first()

    if (!task) throw new HttpException(
      'Cannot find database row',
      404,
      'E_MISSING_DATABASE_ROW'
    )

    task = task.toJSON()

    if (!!task.questions) {
      task.questions = task.questions.map(q => {
        q.answer = q.pivot
        delete q.pivot
        return q
      })
      task.questions = Question.orderQuestionsByGroup(task.questions)
    }

    return task
  }

  async update({ request }) {
    const attributes = request.post()
    let task = await Task.findOrFail(attributes.id)

    task.merge(attributes)
    await task.save()

    return task
  }

  async destroy({ request }) {
    const { id } = request.params
    const task = await Task.findOrFail(id)

    await task.delete()
  }

  async changeStatus({ request }) {
    const { approved } = request.all()
    const { id } = request.params

    const task = await Task.findOrFail(id)

    task.approved = approved
    task.save()

    return task
  }

}

module.exports = TaskController
