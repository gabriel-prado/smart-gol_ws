'use strict'

const S3Service = use('App/Services/S3')
const SendGridService = use('App/Services/SendGrid')

class S3Controller {

  async uploadImage({ request, response }) {
    request.multipart.file('image', {}, async (file) => {
      const url = await S3Service.send(file)

      return response.status(200).send({ url })
    })

    await request.multipart.process()
  }

  async getFile({ request, response }) {
    const { name } = request.all()
    let fileName = name

    if (!fileName && request.match(['/company/bulk-storage'])) {
      fileName = 'bulk-company.xlsx'
    } else if (!fileName && request.match(['/user/bulk-storage'])) {
      fileName = 'bulk-user.xlsx'
    }

    let res = await S3Service.get(fileName)

    response.header('content-type', res.ContentType)
    response.header('content-disposition', fileName)
    return res.Body
  }

}

module.exports = S3Controller
