'use strict'

const TaskQuestion = use('App/Models/TaskQuestion')
const Task = use('App/Models/Task')
const RoleCheckerException = use('App/Exceptions/RoleCheckerException')
const { HttpException } = require('@adonisjs/generic-exceptions')

class TaskQuestionController {

  async index({ response }) {
    return response.status('200').json(await TaskQuestion.all())
  }

  async store({ request, response, auth }) {
    const taskQuestion = request.post()
    const task = await Task.findOrFail(taskQuestion.task_id)

    if ((auth.user.id !== task.user_id) && auth.user.role !== 'BACKOFFICE')
      throw new RoleCheckerException()

    return response.status('201').json(await TaskQuestion.create(taskQuestion))
  }

  async show({ request, auth }) {
    const { id, task_id  } = request.params
    let taskQuestion = await TaskQuestion
      .query()
      .where('question_id', id)
      .where('task_id', task_id)
      .first()

    const task = await Task.findOrFail(!!taskQuestion || null)

    if ((auth.user.id !== task.user_id) && auth.user.role !== 'BACKOFFICE')
      throw new RoleCheckerException()

    return taskQuestion
  }

  async update({ request, auth }) {
    const attributes = request.post()
    let taskQuestion = await TaskQuestion.findOrFail(attributes.id)
    const task = await taskQuestion.task().fetch()

    if ((auth.user.id !== task.user_id) && auth.user.role !== 'BACKOFFICE')
      throw new RoleCheckerException()

    taskQuestion.merge(attributes)
    await taskQuestion.save()

    return taskQuestion
  }

  async destroy({ request }) {
    const { id, task_id } = request.params

    const count = await TaskQuestion
      .query()
      .where('question_id', id)
      .where('task_id', task_id)
      .delete()

    if (count < 1) throw new HttpException(
      'Cannot find database row',
      404, 
      'E_MISSING_DATABASE_ROW'
    )
  }

}

module.exports = TaskQuestionController
