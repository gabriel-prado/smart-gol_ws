'use strict'

const User = use('App/Models/User')
const SendGridService = use('App/Services/SendGrid')
const RoleCheckerException = use('App/Exceptions/RoleCheckerException')

class AuthController {

  async loginPortal({ request, response, auth }) {
    const { email, password } = request.post()
    const { token } = await auth.attempt(email, password)

    if(!!token) {
      const isBackoffice = await User
        .query()
        .where('email', email)
        .where('role', 'BACKOFFICE')
        .getCount() === 1

      if(!isBackoffice) throw new RoleCheckerException()
    }

    return { token : `Bearer ${token}` }
  }

  async loginApp({ request, auth, response }) {
    const { email, password } = request.post()
    const { token } = await auth.attempt(email, password)

    const user = await User.findByOrFail('email', email)

    if (user.status === false) {
      return response.status('403').json({ status:'DENIED', message: user.denied_reason })
    } else if (user.status === null) {
      return response.status('403').json({ status:'PENDING' })
    }

    return { token : `Bearer ${token}` }
  }

  async me({ auth }) {

    if (auth.user.firstAcess) {
      auth.user.firstAcess = false
      await auth.user.save()
      auth.user.firstAcess = true
    }

    await auth.user.loadMany({
      exchanges: (builder) => builder.with('product')
    })

    return auth.user
  }

}

module.exports = AuthController
