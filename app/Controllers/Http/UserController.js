'use strict'

const User = use('App/Models/User')
const SendGridService = use('App/Services/SendGrid')
const RoleCheckerException = use('App/Exceptions/RoleCheckerException')
const { HttpException } = require('@adonisjs/generic-exceptions')
const Route = use('Route')
const generator = require('generate-password')
const BulkStorageService = use('App/Services/BulkStorage')

class UserController {

  async index({ request, response }) {
    const { name } = request.all()

    const user = !!name ?
      await User.query().where('username', 'like', `%${name}%`).fetch() :
      await User.all()

    return response.status('200').json(user)
  }

  async store({ request, response }) {
    const { host } = request.headers()
    const params = request.post()
    let result = true

    if(!params.password) {
      params.password = generator.generate({
        length: 4,
        numbers: false,
        uppercase: true
      })
    }

    const user = await User.create(params)
    await user.reload()

    if (user.role === 'USER') {
      const route = Route.url('/confirm-email', {id: user.id})

      result = await SendGridService.signUpMail({
        email: user.email,
        link: 'http://' + host + route,
        name: user.username
      })
    } else {
      result = await SendGridService.newBackofficeMail({
        email: user.email,
        password: params.password,
        name: user.username
      })
    }

    if(!result || !result[0] || result[0].statusMessage !== 'Accepted') {
      await user.delete()
      throw new HttpException ('Email not send.',500,'E_EMAIL_NOT_SEND')
    }

    return response.status('201').json(user)
  }

  async show({ request }) {
    const { id } = request.params

    return User.findOrFail(id)
  }

  async update({ request, auth }) {
    const attributes = request.post()
    let user = await User.findOrFail(attributes.id)

    if ((auth.user.id !== user.id) && auth.user.role !== 'BACKOFFICE')
      throw new RoleCheckerException()

    user.merge(attributes)
    await user.save()

    return user
  }

  async destroy({ request, auth }){
    const { id } = request.params
    const user = await User.findOrFail(id)

    if ((auth.user.id !== user.id) && auth.user.role !== 'BACKOFFICE')
      throw new RoleCheckerException()

    await user.delete()
  }

  async changeStatus({ request }) {
    const { denied_reason, status} = request.all()
    const { id } = request.params

    const user = await User.findOrFail(id)

    if(!user.isConfirmed) throw new HttpException (
      'Precondition Failed, user email is not confirmed.',
      412,
      'E_USER_NOT_CONFIRMED'
    )

    if (!status) user.denied_reason = denied_reason

    user.status = status
    user.save()

    return user
  }

  async confirmEmail ({ request, response }) {
    const { id } = request.params
    const user = await User.findOrFail(id)
    user.isConfirmed = true
    await user.save()

    response.redirect('/confirmed')
  }

  async changePassword ({ request }) {
    const { email } = request.all()
    const user = await User.findByOrFail('email', email)
    const password = generator.generate({
      length: 4,
      numbers: false,
      uppercase: true
    })

    const result = await SendGridService.changePassword({
      email: user.email,
      name: user.username,
      password
    })

    if(!result || !result[0] || result[0].statusMessage !== 'Accepted')
      throw new HttpException ('Email not send.',500,'E_EMAIL_NOT_SEND')

    user.password = password
    await user.save()
  }

  async updatePassword ({ request, auth }) {
    const { email, oldPassword, password } = request.all()

    try {
      await auth.attempt(auth.user.email, oldPassword)
    } catch (e) {
      throw new HttpException ('Password is not correct.',400,'E_WRONG_PASSWORD')
    }

    auth.user.password = password
    auth.user.save()
  }

  async bulkStorage({ request, response }) {
    const file = request.file('file')

    const res = await BulkStorageService.storage(file, User, 'username', 'bulk-user.xlsx')

    return response.status('200').json(res)
  }

}

module.exports = UserController
