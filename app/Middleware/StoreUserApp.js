'use strict'

class StoreUserApp {
  async handle ({ request }, next) {
    if (request.hasBody()) {
      let user = request.post()
      user.role = 'USER'
      request.body = user
    }

    await next()
  }
}

module.exports = StoreUserApp
