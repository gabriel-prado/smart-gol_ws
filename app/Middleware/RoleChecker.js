'use strict'

const RoleCheckerException = use('App/Exceptions/RoleCheckerException')

class RoleChecker {
  async handle ({ response, auth }, next, roles) {
    if (!(auth.user && roles.includes(auth.user.role))) throw new RoleCheckerException()
    
    return await next()
  }
}

module.exports = RoleChecker
