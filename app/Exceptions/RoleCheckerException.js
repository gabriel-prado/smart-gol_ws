'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')
const message = 'This user does not have access.'
const status = 403
const code = 'E_ROLE_CHECKER'

class RoleCheckerException extends LogicalException {
  constructor () {
    super(message, status, code)
  }
}

module.exports = RoleCheckerException
