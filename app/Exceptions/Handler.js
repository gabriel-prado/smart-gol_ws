'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

class ExceptionHandler extends BaseExceptionHandler {

  async handle (error, { request, response }) {
    // console.error(error.stack)

    response.status(error.status).send({
      showUserMessage: _getUserMessage(error),
      debugMessage: error.message,
      code: error.code
    })
  }

  async report (error, { request }) {
  }
}

ExceptionHandler.getMessage = _getUserMessage

function _getUserMessage(error) {
  switch (error.code) {
    case 'E_PASSWORD_MISMATCH':
      return 'Usuário ou senha estão incorretos.'
      break;
    case 'E_ROLE_CHECKER':
      return 'Usuário não possui acesso.'
      break;
    case 'ER_DUP_ENTRY':
      return 'Não foi possível salvar esse cadastro porque ele já existe.'
      break;
    case 'E_INVALID_JWT_TOKEN':
      return 'Token inválido.'
      break;
    case 'E_USER_NOT_FOUND':
      return 'Usuário não encontrado.'
      break;
    case 'E_VALIDATION_FAILED':
      return _validationMessage(error)
      break;
    case 'E_MISSING_DATABASE_ROW':
      return 'Registro não encontrado.'
      break;
    case 'E_ROUTE_NOT_FOUND':
      return 'Rota não encontrada.'
      break;
    case 'E_USER_NOT_CONFIRMED':
      return 'O email do usuário não foi confirmado.'
      break;
    case 'E_EMAIL_NOT_SEND':
      return 'Houve um erro no envio de email.'
      break;
    case 'E_NOT_ENOUGH_VALUE':
      return 'O usuário não possui pontos suficientes.'
      break;
    case 'E_OUT_OF_STOCK':
      return 'O produto não possui estoque.'
      break;
    case 'E_EXCHANGE_DONE':
      return 'Essa troca já foi aprovada.'
      break;
    case 'ER_TRUNCATED_WRONG_VALUE_FOR_FIELD':
      return error.sqlMessage
      break;
    case 'NoSuchKey':
      return 'O arquivo solicitado não existe.'
      break;
    case 'E_WRONG_PASSWORD':
      return 'A senha antiga está incorreta.'
      break;
    case 'E_TASK_NOT_FINISHED':
      return 'A tarefa não foi finalizada.'
      break;
    default:
      return 'Ocorreu um erro interno.'
  }
}

function _validationMessage(error) {
  let campo = error.messages[0]
  const defaultMessage = 'Ocorreu um erro de validação.'

  // console.error('Erro de validação: ', campo)

  if (!!!campo) return defaultMessage

  switch (campo.validation) {
    case 'unique':
      return `O campo ${campo.field} deve ser único.`
      break;
    case 'email':
      return `O campo ${campo.field} deve estar no formato de email.`
      break;
    case 'required':
      return `O campo ${campo.field} é obrigatório.`
      break;
    case 'requiredWhen':
      return `O campo ${campo.field} é obrigatório.`
      break;
    case 'in':
      return _checkErrorInList(campo.field)
      break;
    case 'number':
      return `O campo ${campo.field} deve ser um número.`
      break;
    case 'alphaNumeric':
      return `O campo ${campo.field} não pode conter caracteres especiais.`
      break;
    case 'array':
      return `O campo ${campo.field} deve ser uma lista.`
      break;
    case 'string':
      return `O campo ${campo.field} deve ser um texto.`
      break;
    case 'date':
      return `O campo ${campo.field} está com o formato de data incorreto.`
      break;
    case 'file':
      return `O campo ${campo.field} deve ser um arquivo.`
      break;
    default:
      return defaultMessage
  }
}

function _checkErrorInList(field) {
  if(field.includes(".")) field = field.split(".", 1)
  return `O valor do campo ${field} não é uma opção válida.`
}

module.exports = ExceptionHandler
