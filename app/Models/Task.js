'use strict'

const Model = use('Model')
const moment = require('moment')

class Task extends Model {

  static boot () {
    super.boot()
    this.addTrait('ClearParams')
  }

  getDateStart(date_start) {
    return moment.isDate(date_start) ? moment(date_start).format('YYYY-MM-DD') : date_start
  }

  getDateFinished(date_finished) {
    return moment.isDate(date_finished) ? moment(date_finished).format('YYYY-MM-DD') : date_finished
  }

  getDateLimit(date_limit) {
    return moment.isDate(date_limit) ? moment(date_limit).format('YYYY-MM-DD') : date_limit
  }

  getDateBonus(date_bonus) {
    return moment.isDate(date_bonus) ? moment(date_bonus).format('YYYY-MM-DD') : date_bonus
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  client() {
    return this.belongsTo('App/Models/Client')
  }

  answers() {
    return this.hasMany('App/Models/TaskQuestion')
  }

  questions() {
    return this
      .belongsToMany('App/Models/Question')
      .pivotModel('App/Models/TaskQuestion')
      .withPivot(['id','task_id','question_id','approved','answer','photos', 'updated_at', 'created_at'])
  }

  company() {
    return this.belongsTo('App/Models/Company')
  }

  static get columns() {
    return [
      'id',
      'user_id',
      'client_id',
      'company_id',
      'description',
      'points',
      'date_start',
      'date_finished',
      'date_limit',
      'date_bonus',
      'points_bonus',
      'approved'
    ]
  }
}

module.exports = Task
