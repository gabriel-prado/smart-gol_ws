'use strict'

const Model = use('Model')

class TaskQuestion extends Model {

  static boot () {
    super.boot()
    this.addTrait('ClearParams')
  }

  getPhotos(photos) {
    return JSON.parse(photos) || []
  }

  setPhotos(photos) {
    return (!!photos) ? JSON.stringify(photos) : '[]'
  }

  task() {
    return this.belongsTo('App/Models/Task', 'task_id')
  }

  question() {
    return this.belongsTo('App/Models/Question', 'question_id')
  }

  static get columns() {
    return [
      'id',
      'task_id',
      'question_id',
      'approved',
      'answer',
      'photos'
    ]
  }

}

module.exports = TaskQuestion
