'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
  static boot () {
    super.boot()

    this.addTrait('ClearParams')
  }

  exchanges() {
    this.belongsToMany('App/Models/Exchange')
  }

  static get columns() {
    return [
      'id',
      'name',
      'photo',
      'amount',
      'points',
      'description'
    ]
  }
}

module.exports = Product
