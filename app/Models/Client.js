'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Client extends Model {
  static boot () {
    super.boot()

    this.addTrait('ClearParams')
  }

  companies() {
    return this.belongsToMany('App/Models/Company').pivotTable('company_clients')
  }

  static get columns() {
    return [
      'id',
      'client_name'
    ]
  }
}

module.exports = Client
