'use strict'

const Model = use('Model')

class Question extends Model {

  static boot () {
    super.boot()
    this.addTrait('ClearParams')
  }

  answer() {
    return this.hasOne('App/Models/TaskQuestion')
  }

  tasks() {
    return this
      .belongsToMany('App/Models/Task')
      .pivotModel('App/Models/TaskQuestion')
      .withPivot(['id','task_id','question_id','approved','answer','photos', 'updated_at', 'created_at'])
  }

  static orderQuestionsByGroup(list) {
    if (!!list.length) {
      let newList = {}
      list.forEach((q, i) => {
          if (!(q.question_group in newList)) {
            newList[q.question_group] = new Array()
          }
          newList[q.question_group].push(q)
      })

      return newList
    }

    return list
  }

  static get columns() {
    return [
      'id',
      'question_title',
      'type',
      'question_group',
      'photos_required',
      'photos_amount',
      'photos_comment_required'
    ]
  }
}

module.exports = Question
