'use strict'

const moment = use('moment')
const Model = use('Model')

class Exchange extends Model {
  static boot () {
    super.boot()

    this.addTrait('ClearParams')
  }

  getDateApproved(date_approved) {
    return moment.isDate(date_approved) ? moment(date_approved).format('YYYY-MM-DD') : date_approved
  }

  getDateExchange(date_exchange) {
    return moment.isDate(date_exchange) ? moment(date_exchange).format('YYYY-MM-DD') : date_exchange
  }

  product() {
    return this.belongsTo('App/Models/Product')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  static get columns() {
    return [
      'id',
      'user_id',
      'product_id',
      'date_approved',
      'date_exchange'
    ]
  }
}

module.exports = Exchange
