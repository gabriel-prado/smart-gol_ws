'use strict'

const Model = use('Model')
const Hash = use('Hash')
const moment = require('moment')

class User extends Model {
  static boot () {
    super.boot()

    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        if (typeof userInstance.password !== 'string') {
          userInstance.password = userInstance.password.toString()
        }
        userInstance.password = await Hash.make(userInstance.password)
      }
    })

    this.addTrait('ClearParams')
  }

  getBirthdate(birthdate) {
    return moment.isDate(birthdate) ? moment(birthdate).format('YYYY-MM-DD') : birthdate
  }

  static get hidden () {
    return ['password']
  }

  getPermissions(permissions) {
    return JSON.parse(permissions) || []
  }

  setPermissions(permissions) {
    return !!permissions ? JSON.stringify(permissions) : '[]'
  }

  tokens () {
    return this.hasMany('App/Models/Token')
  }

  tasks () {
    return this.hasMany('App/Models/Task')
  }

  exchanges () {
    return this.hasMany('App/Models/Exchange')
  }

  static get columns() {
    return [
      'id',
      'username',
      'document',
      'role',
      'permissions',
      'email',
      'password',
      'gender',
      'phone',
      'birthdate',
      'zipcode',
      'address',
      'number',
      'complement',
      'neighborhood',
      'city',
      'state',
      'firstAcess',
      'status',
      'isConfirmed',
      'photo',
      'denied_reason',
      'points'
    ]
  }
}

module.exports = User
