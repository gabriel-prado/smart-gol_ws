'use strict'

const Model = use('Model')

class Company extends Model {
  static boot () {
    super.boot()

    this.addTrait('ClearParams')
  }

  clients() {
    return this.belongsToMany('App/Models/Client').pivotTable('company_clients')
  }

  static get columns() {
    return [
      'id',
      'trading_name',
      'company_name',
      'document',
      'responsible_name',
      'responsible_phone',
      'company_phone',
      'zipcode',
      'address',
      'number',
      'complement',
      'neighborhood',
      'city',
      'state'
    ]
  }
}

module.exports = Company
