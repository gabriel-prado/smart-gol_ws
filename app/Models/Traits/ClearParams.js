'use strict'

class ClearParams {

  async register(Model, customOptions = {}) {
    let validColumns = Model.columns

    Model.clearParams = function (params) {

      for (let param in params) {
        if (!validColumns.includes(param)) delete params[param]
      }

      return params
    }

  }

}

module.exports = ClearParams
