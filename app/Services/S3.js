'use strict'

const Drive = use('Drive')
const moment = require('moment')

class S3Service {

  send(file) {
    const fileName = moment().valueOf().toString() + '.' + file.subtype
    const stream = file.stream
    const params = {
      ACL: 'public-read',
      Key: fileName,
      ContentType: file.headers['content-type']
    }

    return Drive
      .disk('s3')
      .put(fileName, stream, params)
  }

  get(fileName) {
    return Drive.disk('s3').getObject(fileName)
  }

}

module.exports = S3Service
