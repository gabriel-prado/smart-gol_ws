'use strict'

const XLSX = require('xlsx')
const Helpers = use('Helpers')
const ExceptionHandler = use('App/Exceptions/Handler')
const { HttpException } = require('@adonisjs/generic-exceptions')

class BulkStorage {

  async storage(file, Model, entityName, fileName) {
    if (!file) {
      const error = new HttpException('Validation failed', 400, 'E_VALIDATION_FAILED')
      error.messages = [{ field: 'file', validation: 'required'}]
      throw error
    }

    await file.move(Helpers.tmpPath('uploads'), {
      name: fileName,
      overwrite: true
    })

    if (!file.moved()) return file.error()

    const workbook = XLSX.readFile('tmp/uploads/' + fileName);
    const rows = XLSX.utils.sheet_to_json(workbook.Sheets[Object.keys(workbook.Sheets)[0]])

    let errors = []
    let promises = []

    rows.forEach(async (item, i) => { promises.push(Model.create(item)) })

    const caughtPromises = promises.map(promise => promise.catch(e => e));
    const results = await Promise.all(caughtPromises)

    results.forEach((item, i) => {
      if (item instanceof Error) {
        errors.push({
          line: i + 2,
          name: rows[i][entityName],
          message: ExceptionHandler.getMessage(item)
        })
      }
    })

    return {
      sucessCount: rows.length - errors.length,
      errorCount: errors.length,
      errors
    }
  }

}

module.exports = BulkStorage
