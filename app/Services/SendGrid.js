'use strict'

const sgMail = require('@sendgrid/mail')
const Env = use('Env')
sgMail.setApiKey(Env.get('SENDGRID_API_KEY'))

class SendGridService {

  signUpMail(data) {
    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: data.email,
      templateId: Env.get('SIGN_UP_TEMPLATE_ID'),
      dynamic_template_data: {
        name: data.name,
        link: data.link
      }
    }

    return sgMail.send(msg)
  }

  newBackofficeMail(data) {
    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: data.email,
      templateId: Env.get('NEW_BACKOFFICE_TEMPLATE_ID'),
      dynamic_template_data: {
        name: data.name,
        password: data.password
      }
    }

    return sgMail.send(msg)
  }

  changePassword(data) {
    const msg = {
      from: Env.get('DEFAULT_CONTACT_MAIL'),
      to: data.email,
      templateId: Env.get('CHANGE_PASSWORD_TEMPLATE_ID'),
      dynamic_template_data: {
        name: data.name,
        password: data.password
      }
    }

    return sgMail.send(msg)
  }

}

module.exports = SendGridService
