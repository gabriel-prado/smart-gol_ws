'use strict'

const Route = use('Route')

Route.get('/api-specs', 'SwaggerController.index')

Route.group('Authentication', () => {
  Route.post('login-portal', 'AuthController.loginPortal')
  Route.post('login-app', 'AuthController.loginApp')
  Route.get('/me', 'AuthController.me').middleware(['auth', 'acl:BACKOFFICE,USER'])
}).prefix('auth')

Route.group('User Features', () => {
  Route
    .put('/:id/change-status', 'UserController.changeStatus')
    .middleware(['auth','acl:BACKOFFICE'])
    .validator('ChangeStatusUser')
  Route
    .get('/:id/confirm-email', 'UserController.confirmEmail')
    .as('/confirm-email')
  Route
    .put('/change-password', 'UserController.changePassword')
    .validator('ChangePassword')
  Route
    .put('/:id/update-password', 'UserController.updatePassword')
    .validator('UpdatePassword')
    .middleware(['auth','acl:BACKOFFICE,USER'])
  Route
    .post('/bulk-storage', 'UserController.bulkStorage')
    .middleware(['auth', 'acl:BACKOFFICE'])
  Route
    .get('/bulk-storage', 'S3Controller.getFile')
    .middleware(['auth', 'acl:BACKOFFICE'])
}).prefix('user')

Route.group('Company Bulk', () => {
  Route.post('', 'CompanyController.bulkStorage')
  Route.get('', 'S3Controller.getFile')
}).prefix('company/bulk-storage').middleware(['auth', 'acl:BACKOFFICE'])

Route
  .resource('user', 'UserController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware('auth')
  .middleware(new Map([
    [['user.update', 'user.destroy'], ['acl:BACKOFFICE,USER']],
    [['user.index', 'user.show', 'user.store'], ['acl:BACKOFFICE']]
  ]))
  .validator(new Map([
    [['user.store'], ['StoreUser']],
    [['user.update'], ['UpdateUser']]
  ]))

Route
  .resource('user.task', 'UserTaskController')
  .only(['index', 'update'])
  .middleware(['auth', 'acl:BACKOFFICE,USER'])

Route
  .resource('company.task', 'CompanyTaskController')
  .only(['index'])
  .middleware(['auth', 'acl:BACKOFFICE'])

Route
  .post('sign-up', 'UserController.store')
  .middleware('signUp')
  .validator('StoreUser')

Route
  .resource('company', 'CompanyController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware('auth')
  .middleware(new Map([
    [['company.show'], ['acl:BACKOFFICE,USER']],
    [['company.index', 'company.destroy', 'company.store', 'company.update'], ['acl:BACKOFFICE']]
  ]))
  .validator(new Map([
    [['company.store'], ['StoreCompany']],
    [['company.update'], ['UpdateCompany']]
  ]))

Route
  .resource('client', 'ClientController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware(['auth', 'acl:BACKOFFICE'])
  .validator(new Map([
    [['client.store'], ['StoreClient']],
    [['client.update'], ['UpdateClient']]
  ]))

Route
  .resource('task', 'TaskController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware('auth')
  .middleware(new Map([
    [['task.store', 'task.update', 'task.destroy'], ['acl:BACKOFFICE']],
    [['task.index', 'task.show'], ['acl:USER,BACKOFFICE']]
  ]))
  .validator(new Map([
    [['task.store'], ['StoreTask']],
    [['task.update'], ['UpdateTask']]
  ]))

Route
  .resource('question', 'QuestionController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware(['auth', 'acl:BACKOFFICE'])
  .validator(new Map([
    [['question.store'], ['StoreQuestion']],
    [['question.update'], ['UpdateQuestion']]
  ]))

Route
  .resource('task.question', 'TaskQuestionController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware('auth')
  .middleware(new Map([
    [['task.question.store', 'task.question.update', 'task.question.show'], ['acl:USER,BACKOFFICE']],
    [['task.question.destroy', 'task.question.index'], ['acl:BACKOFFICE']]
  ]))
  .validator(new Map([
    [['task.question.store'], ['StoreTaskQuestion']],
    [['task.question.update'], ['UpdateTaskQuestion']]
  ]))

Route
  .resource('product', 'ProductController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware('auth')
  .middleware(new Map([
    [['product.index', 'product.show'], ['acl:USER,BACKOFFICE']],
    [['product.store', 'product.update', 'product.destroy'], ['acl:BACKOFFICE']]
  ]))
  .validator(new Map([
    [['product.store'], ['StoreProduct']],
    [['product.update'], ['UpdateProduct']]
  ]))

Route
  .resource('user.product', 'ExchangeController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware('auth')
  .middleware(new Map([
    [['user.product.store', 'user.product.index', 'user.product.show'], ['acl:USER,BACKOFFICE']],
    [['user.product.update', 'user.product.destroy'], ['acl:BACKOFFICE']]
  ]))
  .validator(new Map([
    [['exchange.store'], ['StoreExchange']],
    [['exchange.update'], ['UpdateExchange']]
  ]))

Route
  .resource('exchange', 'ExchangeController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware(['auth', 'acl:BACKOFFICE'])
  .validator(new Map([
    [['exchange.store'], ['StoreExchange']],
    [['exchange.update'], ['UpdateExchange']]
  ]))

Route
  .put('exchange/:id/change-status', 'ExchangeController.changeStatus')
  .middleware(['auth','acl:BACKOFFICE'])
  .validator('ChangeStatus')

Route
  .put('task/:id/change-status', 'TaskController.changeStatus')
  .middleware(['auth','acl:BACKOFFICE'])
  .validator('ChangeStatus')

Route
  .post('upload-image', 'S3Controller.uploadImage')

Route
  .get('dashboard', 'DashboardController.index')
  .middleware(['auth', 'acl:BACKOFFICE'])

Route.on('confirmed').render('confirmed')
