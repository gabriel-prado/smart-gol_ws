const { hooks } = require('@adonisjs/ignitor')
const { ioc } = require('@adonisjs/fold')

hooks.before.providersBooted(() => {
  if (process.env.NODE_ENV === 'testing') {
    ioc.fake('App/Services/SendGrid', () => {
      return {
        signUpMail: () => [{statusMessage: 'Accepted'}],
        newBackofficeMail: () => [{statusMessage: 'Accepted'}],
        changePassword: () => [{statusMessage: 'Accepted'}]
      }
    })

    ioc.fake('App/Services/S3Service', () => {
      return {
        send: () => ({ url:"http://www.url.com" })
      }
    })
  }
})

hooks.after.providersBooted(() => {
  const SendGridService = require('./../app/Services/SendGrid')
  const S3Service = require('./../app/Services/S3')
  const BulkStorageService = require('./../app/Services/BulkStorage')

  ioc.singleton('App/Services/SendGrid', () => new SendGridService())
  ioc.singleton('App/Services/S3', () => new S3Service())
  ioc.singleton('App/Services/BulkStorage', () => new BulkStorageService())
})
