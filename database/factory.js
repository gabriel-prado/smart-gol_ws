'use strict'

const Factory = use('Factory')

Factory.blueprint('App/Models/User', (faker, i, data) => {
  return {
    username:  _checkValue(data.username, faker.name()),
    document:  _checkValue(data.document, faker.integer(_intSize(11))),
    role:  _checkValue(data.role, faker.pickone(['BACKOFFICE','USER'])),
    email:  _checkValue(data.email, faker.email()),
    password:  _checkValue(data.password, faker.word()),
    gender:  _checkValue(data.gender, faker.pickone(['M', 'F'])),
    phone:  _checkValue(data.phone, faker.integer(_intSize(11))),
    birthdate:  _checkValue(data.birthdate, faker.date()),
    zipcode:  _checkValue(data.zipcode, faker.integer(_intSize(8))),
    address:  _checkValue(data.address, faker.street()),
    number:  _checkValue(data.number, faker.integer({ min: 1, max: 999999 })),
    complement: _isDefined(data.complement, faker.word()),
    neighborhood:  _checkValue(data.neighborhood, faker.word()),
    city:  _checkValue(data.city, faker.city()),
    state:  _checkValue(data.state, faker.state()),
    firstAcess: _isDefined(data.firstAcess, true),
    status: _isDefined(data.status, true),
    isConfirmed: _isDefined(data.isConfirmed, true),
    photo: _checkValue(data.photo, faker.sentence()),
    denied_reason: _isDefined(data.denied_reason, faker.sentence()),
    points: _isDefined(data.points, faker.integer({ min: 0, max: 999999})),
    permissions:  data.permissions === 'ALL' ? permissions :
      _isDefined(data.permissions, faker.pickset(permissions, faker.integer({ min: 0, max: 11 })))
  }
})

Factory.blueprint('App/Models/Company', (faker, i, data) => {
  return {
    trading_name:  _checkValue(data.trading_name, faker.name()),
    company_name:  _checkValue(data.company_name, faker.company()),
    document:  _checkValue(data.document, faker.integer(_intSize(14))),
    responsible_name: _checkValue(data.responsible_name, faker.name()),
    responsible_phone: _checkValue(data.responsible_phone, faker.integer(_intSize(11))),
    company_phone: _checkValue(data.company_phone, faker.integer(_intSize(11))),
    zipcode:  _checkValue(data.zipcode, faker.integer(_intSize(8))),
    address:  _checkValue(data.address, faker.street()),
    number:  _checkValue(data.number, faker.integer({ min: 1, max: 999999 })),
    complement: _isDefined(data.complement, faker.word()),
    neighborhood:  _checkValue(data.neighborhood, faker.word()),
    city:  _checkValue(data.city, faker.city()),
    state:  _checkValue(data.state, faker.state())
  }
})

Factory.blueprint('App/Models/Client', (faker, i, data) => {
  return {
    client_name:  _checkValue(data.client_name, faker.name())
  }
})

Factory.blueprint('App/Models/Task', (faker, i, data) => {
  return {
    user_id: _isDefined(data.user_id, null),
    client_id: _checkValue(data.client_id, _generateModel('Client')),
    company_id: _checkValue(data.company_id, _generateModel('Company')),
    description: _checkValue(data.description, faker.paragraph()),
    points: _checkValue(data.points, faker.integer({ min: 1, max: 999999 })),
    date_start: _isDefined(data.date_start, faker.date()),
    date_finished: _isDefined(data.date_finished, faker.date()),
    date_limit: _checkValue(data.date_limit, faker.date()),
    date_bonus: _isDefined(data.date_bonus, faker.date()),
    points_bonus: _isDefined(data.points_bonus, faker.integer({ min: 1, max: 999999 })),
    approved: _isDefined(data.approved, faker.bool())
  }
})

Factory.blueprint('App/Models/Question', (faker, i, data) => {
  return {
    question_title: _checkValue(data.question_title, faker.sentence()),
    type: _checkValue(data.type, faker.pickone(['MULTIPLE_CHOICE','TEXT'])),
    question_group: _checkValue(data.question_group, faker.string()),
    photos_required: _isDefined(data.photos_required, faker.bool()),
    photos_amount: _checkValue(data.photos_amount, faker.integer({ min: 1, max: 99 })),
    photos_comment_required: _isDefined(data.photos_comment_required, faker.bool())
  }
})

Factory.blueprint('App/Models/TaskQuestion', (faker, i, data) => {
  return {
    task_id: _checkValue(data.task_id, _generateModel('Task')),
    question_id: _checkValue(data.question_id, _generateModel('Question')),
    approved: _isDefined(data.approved, faker.bool()),
    answer: _isDefined(data.answer, faker.sentence()),
    photos: _isDefined(data.photos, [])
  }
})

Factory.blueprint('App/Models/Product', (faker, i, data) => {
  return {
    name: _isDefined(data.name, faker.name()),
    photo: _checkValue(data.photo, faker.sentence()),
    amount: _isDefined(data.amount, faker.integer({ min: 1, max: 99999 })),
    points: _isDefined(data.points, faker.integer({ min: 1, max: 99999 })),
    description: _checkValue(data.description, faker.paragraph())
  }
})

Factory.blueprint('App/Models/Exchange', (faker, i, data) => {
  return {
    user_id: _checkValue(data.user_id, _generateModel('User')),
    product_id: _checkValue(data.product_id, _generateModel('Product')),
    date_approved: _isDefined(data.date_approved, faker.date()),
    date_exchange: _isDefined(data.date_exchange, faker.date()),
  }
})

const _checkValue = (data, fake) => {
  return (data === null || data === undefined || data === NaN || data === "" ) ? fake : data
}

const _isDefined = (data, fake) => {
  return (data === undefined) ? fake : data
}

const _generateModel = async (model) => {
  const obj = await Factory.model('App/Models/' + model).create()

  return obj.id
}

const _intSize = amount => {
  let size = {
    min: '1',
    max: '9'
  }

  for (var i = 1; i < amount; i++) {
    size.min = size.min + '0'
    size.max = size.max + '9'
  }

  size.min = Number(size.min)
  size.max = Number(size.max)

  return size
}

const permissions = [
  'USER_SHOW',
  'USER_APPROVE',
  'USER_CREATE',
  'COMPANY_SHOW',
  'COMPANY_CREATE',
  'TASK_SHOW_APPROVE',
  'TASK_SEND',
  'TASK_CREATE',
  'EXCHANGE_SHOW',
  'EXCHANGE_APPROVE',
  'EXCHANGE_CREATE_PRODUCTS'
]
