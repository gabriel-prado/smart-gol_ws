'use strict'

const Factory = use('Factory')

class CreateDefaultAdminUserSeeder {
  async run () {
    await Factory.model('App/Models/User').create({
      username: 'Administrador',
      document: 19659109040,
      role: 'BACKOFFICE',
      email: 'admin@smartgol.com',
      password: 'Smartgol!@#123...',
      gender: 'M',
      phone: 34999999999,
      birthdate: '2020-01-01',
      zipcode: 38408584,
      address: 'R. São Judas Tadeu',
      number: 360,
      complement: null,
      neighborhood: 'Carajás',
      city: 'Uberlândia',
      state: 'MG',
      permissions: [
        "USER_SHOW",
        "USER_APPROVE",
        "USER_CREATE",
        "COMPANY_SHOW",
        "COMPANY_CREATE",
        "TASK_SHOW_APPROVE",
        "TASK_SEND",
        "TASK_CREATE",
        "EXCHANGE_SHOW",
        "EXCHANGE_APPROVE",
        "EXCHANGE_CREATE_PRODUCTS"
      ]
    })
  }
}

module.exports = CreateDefaultAdminUserSeeder
