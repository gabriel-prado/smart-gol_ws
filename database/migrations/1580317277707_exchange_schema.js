'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ExchangeSchema extends Schema {
  up () {
    this.create('exchanges', (table) => {
      table.increments()
      table.integer('user_id').notNullable().unsigned()
      table.foreign('user_id').references('users.id')
      table.integer('product_id').notNullable().unsigned()
      table.foreign('product_id').references('products.id')
      table.date('date_approved')
      table.date('date_exchange')
      table.timestamps()
    })
  }

  down () {
    this.drop('exchanges')
  }
}

module.exports = ExchangeSchema
