'use strict'

const Schema = use('Schema')

class TaskQuestionsSchema extends Schema {
  up () {
    this.create('task_questions', (table) => {
      table.increments()
      table.integer('task_id').notNullable().unsigned()
      table.foreign('task_id').references('tasks.id')
      table.integer('question_id').notNullable().unsigned()
      table.foreign('question_id').references('questions.id')
      table.boolean('approved')
      table.text('answer')
      table.json('photos')
      table.timestamps()
    })
  }

  down () {
    this.drop('task_questions')
  }
}

module.exports = TaskQuestionsSchema
