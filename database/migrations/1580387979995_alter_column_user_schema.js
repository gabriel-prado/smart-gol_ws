'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterColumnUserSchema extends Schema {
  up () {
    this.alter('users', (table) => {
      table.string('document', 20).notNullable().alter()
    })
  }

  down () {
    this.alter('users', (table) => {
      table.bigInteger('document', 20).notNullable().unsigned().alter()
    })
  }
}

module.exports = AlterColumnUserSchema
