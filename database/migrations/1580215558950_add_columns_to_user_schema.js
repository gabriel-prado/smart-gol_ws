'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnsToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.boolean('isConfirmed').defaultTo(false)
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('isConfirmed')
    })
  }
}

module.exports = AddColumnsToUserSchema
