'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnsToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.boolean('firstAcess').defaultTo(true)
      table.boolean('status')
      table.text('photo')
      table.text('denied_reason')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('firstAcess')
      table.dropColumn('status')
      table.dropColumn('photo')
      table.dropColumn('denied_reason')
    })
  }
}

module.exports = AddColumnsToUserSchema
