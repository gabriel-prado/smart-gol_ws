'use strict'

const Schema = use('Schema')

class TaskSchema extends Schema {
  up () {
    this.create('tasks', (table) => {
      table.increments()
      table.integer('user_id').unsigned()
      table.foreign('user_id').references('users.id')
      table.integer('client_id').notNullable().unsigned()
      table.foreign('client_id').references('clients.id')
      table.integer('company_id').notNullable().unsigned()
      table.foreign('company_id').references('companies.id')
      table.text('description')
      table.integer('points').notNullable().unsigned().defaultTo(0)
      table.date('date_start')
      table.date('date_finished')
      table.date('date_limit').notNullable()
      table.date('date_bonus')
      table.integer('points_bonus').unsigned()
      table.boolean('approved')
      table.timestamps()
    })
  }

  down () {
    this.drop('tasks')
  }
}

module.exports = TaskSchema
