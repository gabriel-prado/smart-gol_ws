'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable()
      table.bigInteger('document', 20).notNullable().unsigned().unique()
      table.enum('role', ['BACKOFFICE','USER']).notNullable()
      table.json('permissions')
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('gender', 10).notNullable()
      table.bigInteger('phone', 15).notNullable()
      table.date('birthdate').notNullable()
      table.bigInteger('zipcode', 10).unsigned().notNullable()
      table.string('address', 200).notNullable()
      table.integer('number', 10).unsigned().notNullable()
      table.string('complement', 80)
      table.string('neighborhood', 200).notNullable()
      table.string('city', 200).notNullable()
      table.string('state', 200).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
