'use strict'

const Schema = use('Schema')

class QuestionSchema extends Schema {
  up () {
    this.create('questions', (table) => {
      table.increments()
      table.text('question_title').notNullable()
      table.enum('type', ['MULTIPLE_CHOICE','TEXT']).notNullable()
      table.string('question_group')
      table.boolean('photos_required').notNullable().defaultTo(false)
      table.integer('photos_amount').notNullable().defaultTo(0)
      table.boolean('photos_comment_required').notNullable().defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('questions')
  }
}

module.exports = QuestionSchema
