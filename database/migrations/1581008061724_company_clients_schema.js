'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompanyClientsSchema extends Schema {
  up () {
    this.create('company_clients', (table) => {
      table.increments()
      table.integer('company_id').notNullable().unsigned()
      table.foreign('company_id').references('companies.id')
      table.integer('client_id').notNullable().unsigned()
      table.foreign('client_id').references('clients.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('company_clients')
  }
}

module.exports = CompanyClientsSchema
