'use strict'

const Schema = use('Schema')

class CompanySchema extends Schema {
  up () {
    this.create('companies', (table) => {
      table.increments()
      table.string('trading_name', 80).notNullable()
      table.string('company_name', 80).notNullable()
      table.bigInteger('document', 20).notNullable().unsigned().unique()
      table.string('responsible_name', 80).notNullable()
      table.bigInteger('responsible_phone').notNullable()
      table.bigInteger('company_phone').notNullable()
      table.bigInteger('zipcode', 10).unsigned().notNullable()
      table.string('address', 200).notNullable()
      table.integer('number', 10).unsigned().notNullable()
      table.string('complement', 80)
      table.string('neighborhood', 200).notNullable()
      table.string('city', 200).notNullable()
      table.string('state', 200).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('companies')
  }
}

module.exports = CompanySchema
