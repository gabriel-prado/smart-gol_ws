'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.integer('points').notNullable().defaultTo(0)
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('points')
    })
  }
}

module.exports = AddColumnToUserSchema
