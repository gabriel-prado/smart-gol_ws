const Database = use('Database')

module.exports = async () => {

  await Database.transaction(async (trx) => {
    await trx.raw('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;')
    await trx.raw('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;')
    await trx.raw('SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE="TRADITIONAL,ALLOW_INVALID_DATES";')

    await trx.truncate('users')
    await trx.truncate('tokens')
    await trx.truncate('company_clients')
    await trx.truncate('companies')
    await trx.truncate('clients')
    await trx.truncate('task_questions')
    await trx.truncate('tasks')
    await trx.truncate('questions')
    await trx.truncate('exchanges')
    await trx.truncate('products')

    await trx.raw('SET SQL_MODE=@OLD_SQL_MODE;')
    await trx.raw('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;')
    await trx.raw('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;')
  })

}
