'use strict'

const Factory = use('Factory')
const Task = use('App/Models/Task')
const { test, trait, beforeEach } = use('Test/Suite')('TaskQuestion - GET /task/id/question')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let task = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  task = await Factory.model('App/Models/Task').create()
  await Factory.model('App/Models/TaskQuestion').createMany(5, {
    task_id: task.id
  })
})

test('should list task-question without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.get(`task/${task.id}/question`).end()

  assert.equal(response.status, 401)
})

test('should list all task-question', async ({ client, assert }) => {
  const response = await client.get(`task/${task.id}/question`).loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 5)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.get(`task/${task.id}/question`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
