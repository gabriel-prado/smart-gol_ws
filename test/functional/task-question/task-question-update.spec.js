'use strict'

const Factory = use('Factory')
const TaskQuestion = use('App/Models/TaskQuestion')
const { test, trait, beforeEach } = use('Test/Suite')('TaskQuestion - PUT /task/id/question/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let task = null
let question = null
let taskQuestion = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  task = await Factory.model('App/Models/Task').create()
  question = await Factory.model('App/Models/Question').create()
  taskQuestion = await Factory.model('App/Models/TaskQuestion').create({
    task_id: task.id,
    question_id: question.id
  })
  taskQuestion = taskQuestion.toJSON()
})

test('should update existent task-question by backoffice', async({ client, assert}) => {
  taskQuestion.answer = 'answer_response'
  const response = await client.put(`task/${task.id}/question/${question.id}`).loginVia(backoffice).send(taskQuestion).end()

  assert.equal(response.status, 200)
  const taskQuestionInDatabase = await TaskQuestion.find(taskQuestion.id)
  assert.isNotNull(taskQuestionInDatabase)
  assert.equal(taskQuestionInDatabase.answer, 'answer_response')
  assert.containsAllKeys(response.body, TaskQuestion.columns)
  assert.equal(response.body.task_id, task.id)
  assert.equal(response.body.question_id, question.id)
})

test('should update existent task-question by userApp', async({ client, assert}) => {
  const userApp = await Factory.model('App/Models/User').create({role: 'USER'})
  task.user_id = userApp.id
  await task.save()
  taskQuestion.answer = 'answer_response'
  const response = await client.put(`task/${task.id}/question/${question.id}`).loginVia(userApp).send(taskQuestion).end()

  assert.equal(response.status, 200)
  const taskQuestionInDatabase = await TaskQuestion.find(taskQuestion.id)
  assert.isNotNull(taskQuestionInDatabase)
  assert.equal(taskQuestionInDatabase.answer, 'answer_response')
  assert.containsAllKeys(response.body, TaskQuestion.columns)
  assert.equal(response.body.task_id, task.id)
  assert.equal(response.body.question_id, question.id)
})

test('should get 403 for update a task-question of another user', async({ client, assert}) => {
  const userApp = await Factory.model('App/Models/User').create({role: 'USER'})
  const response = await client.put(`task/${task.id}/question/${question.id}`).loginVia(userApp).send(taskQuestion).end()

  assert.equal(response.status, 403)
})

test('should get 404 for a non existent task-question', async({ client, assert}) => {
  taskQuestion.id = 12
  const response = await client.put(`task/${task.id}/question/12`).loginVia(backoffice).send(taskQuestion).end()

  assert.equal(response.status, 404)
})

test('should not get task-question for not logged user', async({ client, assert}) => {
  const response = await client.put(`task/${task.id}/question/${question.id}`).send(taskQuestion).end()

  assert.equal(response.status, 401)
})
