'use strict'

const Factory = use('Factory')
const TaskQuestion = use('App/Models/TaskQuestion')
const { test, trait, beforeEach } = use('Test/Suite')('TaskQuestion - POST /task/id/question')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let task = null
let question = null
let taskQuestion = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  task = await Factory.model('App/Models/Task').create()
  question = await Factory.model('App/Models/Question').create()
  taskQuestion = await Factory.model('App/Models/TaskQuestion').make({
    task_id: task.id,
    question_id: question.id
  })
  taskQuestion = taskQuestion.toJSON()
})

test('should store task-question and retrieve 201 by backoffice', async ({ client, assert }) => {
  const response = await client.post(`task/${task.id}/question`).loginVia(backoffice).send(taskQuestion).end()

  assert.equal(response.status, 201)
  const taskQuestionInDatabase = await TaskQuestion.find(response.body.id)
  assert.isNotNull(taskQuestionInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, TaskQuestion.columns)
})

test('should store task-question and retrieve 201 by userApp', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({role: 'USER'})
  task.user_id = userApp.id
  await task.save()
  const response = await client.post(`task/${task.id}/question`).loginVia(userApp).send(taskQuestion).end()

  assert.equal(response.status, 201)
  const taskQuestionInDatabase = await TaskQuestion.find(response.body.id)
  assert.isNotNull(taskQuestionInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, TaskQuestion.columns)
})

test('should get 403 for store a task-question of another user', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({role: 'USER'})
  const response = await client.post(`task/${task.id}/question`).loginVia(userApp).send(taskQuestion).end()

  assert.equal(response.status, 403)
})

test('should store invalid task-question and retrieve 400', async ({ client, assert }) => {
  delete taskQuestion.task_id;

  const response = await client.post(`task/${task.id}/question`).loginVia(backoffice).send(taskQuestion).end()

  assert.equal(response.status, 400)
})

test('should store task-question without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.post(`task/${task.id}/question`).send(taskQuestion).end()

  assert.equal(response.status, 401)
})
