'use strict'

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Authentication - POST /auth/login-portal')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should authenticate user by portal', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    password: 'smartGol'
  })

  const response = await client.post('auth/login-portal').send({
    email: user.email,
    password: 'smartGol'
  }).end()

  assert.equal(response.status, 200)
  assert.hasAllKeys(response.body, ['token'])
})
