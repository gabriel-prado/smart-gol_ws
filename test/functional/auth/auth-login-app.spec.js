'use strict'

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Authentication - POST /auth/login-app')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should authenticate user by app', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'smartGol'
  })

  const response = await client.post('auth/login-app').send({
    email: user.email,
    password: 'smartGol'
  }).end()

  assert.equal(response.status, 200)
  assert.hasAllKeys(response.body, ['token'])
})

test('must fail authentication and return status PENDING', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'smartGol',
    status: null
  })

  const response = await client.post('auth/login-app').send({
    email: user.email,
    password: 'smartGol'
  }).end()

  assert.equal(response.status, 403)
  assert.hasAllKeys(response.body, ['status'])
  assert.equal(response.body.status, 'PENDING')
})

test('must fail authentication and return status DENIED', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'smartGol',
    status: false
  })

  const response = await client.post('auth/login-app').send({
    email: user.email,
    password: 'smartGol'
  }).end()

  assert.equal(response.status, 403)
  assert.hasAllKeys(response.body, ['status', 'message'])
  assert.equal(response.body.status, 'DENIED')
})
