'use strict'

const Factory = use('Factory')
const Company = use('App/Models/Company')
const { test, trait, beforeEach } = use('Test/Suite')('Company - DELETE /company/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
})

test('should destroy company by id', async ({ client, assert }) => {
  const company = await Factory.model('App/Models/Company').create()
  const response = await client.delete(`company/${company.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 204)
  const companyInDatabase =  await Company.find(company.id)
  assert.isNull(companyInDatabase)
})

test('should try to destroy non-existent company and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`company/123`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should try destroy company without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`company/12`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.delete(`company/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
