'use strict'

const Factory = use('Factory')
const Company = use('App/Models/Company')
const { test, trait, beforeEach } = use('Test/Suite')('Company - GET /company/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let company = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  company = await Factory.model('App/Models/Company').create()
})

test('should retrieve existent company', async({ client, assert}) => {
  const response = await client.get(`company/${company.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, Company.columns)
  assert.equal(response.body.id, company.id)
})

test('should get 404 for a non existent company', async({ client, assert}) => {
  const response = await client.get(`company/10`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should not get company for not logged user', async({ client, assert}) => {
  const response = await client.get(`company/${company.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 200', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.get('company/1').loginVia(userApp).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, Company.columns)
  assert.equal(response.body.id, company.id)
})
