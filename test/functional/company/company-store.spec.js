'use strict'

const Factory = use('Factory')
const Company = use('App/Models/Company')
const { test, trait, beforeEach } = use('Test/Suite')('Company - POST /company')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let company = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  company = await Factory.model('App/Models/Company').make()
})

test('should store company and retrieve 201', async ({ client, assert }) => {
  const response = await client.post('company').loginVia(backoffice).send(company.toJSON()).end()

  assert.equal(response.status, 201)
  const companyInDatabase = await Company.find(response.body.id)
  assert.isNotNull(companyInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Company.columns)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })

  const response = await client.post('company').loginVia(userApp).end()
  assert.equal(response.status, 403)
})

test('should store invalid company and retrieve 400', async ({ client, assert }) => {
  const companyJson = company.toJSON()
  delete companyJson.document;

  const response = await client.post('company').loginVia(backoffice).send(companyJson).end()

  assert.equal(response.status, 400)
})

test('should store company without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.post('company').send(company.toJSON()).end()

  assert.equal(response.status, 401)
})
