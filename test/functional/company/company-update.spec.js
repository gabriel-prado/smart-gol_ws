'use strict'

const Factory = use('Factory')
const Company = use('App/Models/Company')
const { test, trait, beforeEach } = use('Test/Suite')('Company - PUT /company')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let company = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  company = await Factory.model('App/Models/Company').create()
})

test('should update company and retrieve 200', async ({ client, assert }) => {
  const companyJson = company.toJSON()
  companyJson.trading_name = 'new_trading_name'

  const response = await client.put(`company/${company.id}`).loginVia(backoffice).send(companyJson).end()

  assert.equal(response.status, 200)
  const companyInDatabase = await Company.find(response.body.id)
  assert.isNotNull(companyInDatabase)
  assert.equal(companyInDatabase.trading_name, 'new_trading_name')
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Company.columns)
})

test('should update invalid company and retrieve 400', async ({ client, assert }) => {
  const companyJson = company.toJSON()
  companyJson.document = "is_not_a_number"

  const response = await client.put(`company/${company.id}`).loginVia(backoffice).send(companyJson).end()

  assert.equal(response.status, 400)
})

test('should update company without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.put(`company/${company.id}`).send(company.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.put(`company/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
