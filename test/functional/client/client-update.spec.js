'use strict'

const Factory = use('Factory')
const Client = use('App/Models/Client')
const { test, trait, beforeEach } = use('Test/Suite')('Client - PUT /client')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let clientObj = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  clientObj = await Factory.model('App/Models/Client').create()
})

test('should update client and retrieve 200', async ({ client, assert }) => {
  const clientJson = clientObj.toJSON()
  clientJson.client_name = 'new_client_name'

  const response = await client.put(`client/${clientObj.id}`).loginVia(backoffice).send(clientJson).end()

  assert.equal(response.status, 200)
  const clientInDatabase = await Client.find(response.body.id)
  assert.isNotNull(clientInDatabase)
  assert.equal(clientInDatabase.client_name, 'new_client_name')
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Client.columns)
})

test('should update invalid client and retrieve 400', async ({ client, assert }) => {
  const clientJson = clientObj.toJSON()
  clientJson.client_name = 123456

  const response = await client.put(`client/${clientObj.id}`).loginVia(backoffice).send(clientJson).end()

  assert.equal(response.status, 400)
})

test('should update client without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.put(`client/${clientObj.id}`).send(clientObj.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.put(`client/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
