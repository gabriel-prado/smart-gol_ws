'use strict'

const Factory = use('Factory')
const Client = use('App/Models/Client')
const { test, trait, beforeEach } = use('Test/Suite')('Client - DELETE /client/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
})

test('should destroy client by id', async ({ client, assert }) => {
  const clientObj = await Factory.model('App/Models/Client').create()
  const response = await client.delete(`client/${clientObj.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 204)
  const clientInDatabase =  await Client.find(clientObj.id)
  assert.isNull(clientInDatabase)
})

test('should try to destroy non-existent client and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`client/123`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should try destroy client without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`client/12`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.delete(`client/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
