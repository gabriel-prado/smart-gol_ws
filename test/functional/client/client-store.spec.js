'use strict'

const Factory = use('Factory')
const Client = use('App/Models/Client')
const { test, trait, beforeEach } = use('Test/Suite')('Client - POST /client')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let clientObj = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  clientObj = await Factory.model('App/Models/Client').make()
})

test('should store client and retrieve 201', async ({ client, assert }) => {
  const response = await client.post('client').loginVia(backoffice).send(clientObj.toJSON()).end()
  
  assert.equal(response.status, 201)
  const clientInDatabase = await Client.find(response.body.id)
  assert.isNotNull(clientInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Client.columns)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })

  const response = await client.post('client').loginVia(userApp).end()
  assert.equal(response.status, 403)
})

test('should store invalid client and retrieve 400', async ({ client, assert }) => {
  const clientJson = clientObj.toJSON()
  delete clientJson.client_name;

  const response = await client.post('client').loginVia(backoffice).send(clientJson).end()

  assert.equal(response.status, 400)
})

test('should store client without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.post('client').send(clientObj.toJSON()).end()

  assert.equal(response.status, 401)
})
