'use strict'

const Factory = use('Factory')
const Client = use('App/Models/Client')
const { test, trait, beforeEach } = use('Test/Suite')('Client - GET /client/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let clientObj = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  clientObj = await Factory.model('App/Models/Client').create()
})

test('should retrieve existent client', async({ client, assert}) => {
  const response = await client.get(`client/${clientObj.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, Client.columns)
  assert.equal(response.body.id, clientObj.id)
})

test('should get 404 for a non existent client', async({ client, assert}) => {
  const response = await client.get(`client/10`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should not get client for not logged user', async({ client, assert}) => {
  const response = await client.get(`client/${clientObj.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.delete(`client/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
