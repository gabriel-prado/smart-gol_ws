'use strict'

const Factory = use('Factory')
const Client = use('App/Models/Client')
const { test, trait, beforeEach } = use('Test/Suite')('Client - GET /client')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  await Factory.model('App/Models/Client').createMany(5)
})

test('should list clients without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.get('client').end()

  assert.equal(response.status, 401)
})

test('should list all clients', async ({ client, assert }) => {
  const response = await client.get('client').loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 5)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.get('client').loginVia(userApp).end()

  assert.equal(response.status, 403)
})
