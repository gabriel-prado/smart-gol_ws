'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const Exchange = use('App/Models/Exchange')
const moment = use('moment')
const { test, trait, beforeEach } = use('Test/Suite')('Exchange - PUT /exchange/id/change-status')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let userApp = null
let exchange = null
let product = null
let now = moment().format('YYYY-MM-DD')

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  userApp = await Factory.model('App/Models/User').create({
    role: 'USER',
    points: 100
  })
  product = await Factory.model('App/Models/Product').create({
    amount: 1,
    points: 100
  })
  exchange = await Factory.model('App/Models/Exchange').create({
    date_approved: null,
    user_id: userApp.id,
    product_id: product.id,
    date_approved: null
  })
})

test('should change exchange status to true and retrieve 200', async ({ client, assert }) => {
  const response = await client
    .put(`exchange/${exchange.id}/change-status`)
    .loginVia(backoffice)
    .send({
      approved: true
    })
    .end()

  assert.equal(response.status, 200)
  assert.isNotNull(response.body)
  assert.equal(response.body.date_approved, now)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = Exchange.columns
  assert.containsAllKeys(response.body, columns)
})

test('should change exchange without stock and retrieve 500', async ({ client, assert }) => {
  product.amount = 0
  await product.save()
  const response = await client
    .put(`exchange/${exchange.id}/change-status`)
    .loginVia(backoffice)
    .send({
      approved: true
    })
    .end()

  assert.equal(response.status, 500)
  assert.equal(response.body.debugMessage, 'E_OUT_OF_STOCK: This product dont have stock.')
})

test('should change exchange without points and retrieve 500', async ({ client, assert }) => {
  userApp.points = 50
  await userApp.save()
  const response = await client
    .put(`exchange/${exchange.id}/change-status`)
    .loginVia(backoffice)
    .send({
      approved: true
    })
    .end()

  assert.equal(response.status, 500)
  assert.equal(response.body.debugMessage, 'E_NOT_ENOUGH_VALUE: User dont have enough points.')
})

test('should change exchange already accepted and retrieve 500', async ({ client, assert }) => {
  exchange.date_approved = now
  await exchange.save()
  const response = await client
    .put(`exchange/${exchange.id}/change-status`)
    .loginVia(backoffice)
    .send({
      approved: true
    })
    .end()

  assert.equal(response.status, 500)
  assert.equal(response.body.debugMessage, 'E_EXCHANGE_DONE: This exchange has already been approved.')
})

test('should change exchange status to false and retrieve 200', async ({ client, assert }) => {
  const response = await client
    .put(`exchange/${exchange.id}/change-status`)
    .loginVia(backoffice)
    .send({
      approved: false
    })
    .end()

  assert.equal(response.status, 200)
  assert.isNotNull(response.body)
  assert.equal(response.body.approved, null)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = Exchange.columns
  assert.containsAllKeys(response.body, columns)
})

test('should change exchange without status and retrieve 400', async ({ client, assert }) => {
  const response = await client
    .put(`exchange/${exchange.id}/change-status`)
    .loginVia(backoffice)
    .end()

  assert.equal(response.status, 400)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.put(`exchange/${exchange.id}/change-status`).loginVia(anotherUser).end()

  assert.equal(response.status, 403)
})

test('should update user without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.put(`exchange/${exchange.id}/change-status`).send(userApp).end()

  assert.equal(response.status, 401)
})
