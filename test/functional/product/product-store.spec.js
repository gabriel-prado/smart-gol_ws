'use strict'

const Factory = use('Factory')
const Product = use('App/Models/Product')
const { test, trait, beforeEach } = use('Test/Suite')('Product - POST /product')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let product = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  product = await Factory.model('App/Models/Product').make()
})

test('should store product and retrieve 201', async ({ client, assert }) => {
  const response = await client.post('product').loginVia(backoffice).send(product.toJSON()).end()

  assert.equal(response.status, 201)
  const productInDatabase = await Product.find(response.body.id)
  assert.isNotNull(productInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Product.columns)
})

test('should store invalid product and retrieve 400', async ({ client, assert }) => {
  const productJson = product.toJSON()
  delete productJson.amount;

  const response = await client.post('product').loginVia(backoffice).send(productJson).end()

  assert.equal(response.status, 400)
})

test('should store product without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.post('product').send(product.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.post('product').loginVia(userApp).end()

  assert.equal(response.status, 403)
})
