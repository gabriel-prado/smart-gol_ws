'use strict'

const Factory = use('Factory')
const Product = use('App/Models/Product')
const { test, trait, beforeEach } = use('Test/Suite')('Product - PUT /product')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let product = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  product = await Factory.model('App/Models/Product').create()
})

test('should update product and retrieve 200', async ({ client, assert }) => {
  const productJson = product.toJSON()
  productJson.amount = 631

  const response = await client.put(`product/${product.id}`).loginVia(backoffice).send(productJson).end()

  assert.equal(response.status, 200)
  const productInDatabase = await Product.find(response.body.id)
  assert.isNotNull(productInDatabase)
  assert.equal(productInDatabase.amount, 631)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Product.columns)
})

test('should update invalid product and retrieve 400', async ({ client, assert }) => {
  const productJson = product.toJSON()
  productJson.amount = 'wrong_value'

  const response = await client.put(`product/${product.id}`).loginVia(backoffice).send(productJson).end()

  assert.equal(response.status, 400)
})

test('should update product without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.put(`product/${product.id}`).send(product.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.put(`product/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
