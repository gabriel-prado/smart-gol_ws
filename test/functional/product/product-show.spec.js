'use strict'

const Factory = use('Factory')
const Product = use('App/Models/Product')
const { test, trait, beforeEach } = use('Test/Suite')('Product - GET /product/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let product = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  product = await Factory.model('App/Models/Product').create()
})

test('should retrieve existent product by backoffice', async({ client, assert}) => {
  const response = await client.get(`product/${product.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, Product.columns)
  assert.equal(response.body.id, product.id)
})

test('should retrieve existent product by userApp', async({ client, assert}) => {
  const userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })
  const response = await client.get(`product/${product.id}`).loginVia(userApp).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, Product.columns)
  assert.equal(response.body.id, product.id)
})

test('should get 404 for a non existent product', async({ client, assert}) => {
  const response = await client.get(`product/10`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should not get product for not logged user', async({ client, assert}) => {
  const response = await client.get(`product/${product.id}`).end()

  assert.equal(response.status, 401)
})
