'use strict'

const Factory = use('Factory')
const Product = use('App/Models/Product')
const { test, trait, beforeEach } = use('Test/Suite')('Product - GET /product')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
  await Factory.model('App/Models/Product').createMany(5)
})

test('should list products without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.get('product').end()

  assert.equal(response.status, 401)
})

test('should list all products by backoffice', async ({ client, assert }) => {
  const backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  const response = await client.get('product').loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 5)
})

test('should list all products by userApp', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })
  const response = await client.get('product').loginVia(userApp).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 5)
})
