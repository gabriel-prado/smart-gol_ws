'use strict'

const Factory = use('Factory')
const Product = use('App/Models/Product')
const { test, trait, beforeEach } = use('Test/Suite')('Product - DELETE /product/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
})

test('should destroy product by id', async ({ client, assert }) => {
  const product = await Factory.model('App/Models/Product').create()
  const response = await client.delete(`product/${product.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 204)
  const productInDatabase =  await Product.find(product.id)
  assert.isNull(productInDatabase)
})

test('should try to destroy non-existent product and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`product/123`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should try destroy product without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`product/12`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.delete(`product/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
