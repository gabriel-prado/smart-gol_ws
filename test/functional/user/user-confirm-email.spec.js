'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - GET /user/id/confirm-email')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let userApp = null

beforeEach(async () => {
  await clearDataBase()
  userApp = await Factory.model('App/Models/User').create({
    role: 'USER',
    isConfirmed: false
  })
})

test('should change user mail confirmed to true and retrieve 200', async ({ client, assert }) => {
  const response = await client.get(`user/${userApp.id}/confirm-email`).end()

  assert.equal(response.status, 200)
  assert.equal(userApp.isConfirmed, false)
  const userInDatabase = await User.find(userApp.id)
  assert.equal(userInDatabase.isConfirmed, true)
})
