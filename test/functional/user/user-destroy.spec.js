'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - DELETE /user/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
})

test('should destroy user by id', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create()
  const response = await client.delete(`user/${user.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 204)
  const userInDatabase =  await User.find(user.id)
  assert.isNull(userInDatabase)
})

test('should destroy user by app', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create()
  const response = await client.delete(`user/${user.id}`).loginVia(user).end()

  assert.equal(response.status, 204)
  const userInDatabase =  await User.find(user.id)
  assert.isNull(userInDatabase)
})

test('should try to destroy non-existent user and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`user/123`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should try destroy user without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`user/12`).end()

  assert.equal(response.status, 401)
})

test('should try destroy another user by app and retrieve 403', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({role: 'USER'})
  const anotherUser = await Factory.model('App/Models/User').create({role: 'USER'})
  const response = await client.delete(`user/${anotherUser.id}`).loginVia(user).end()

  assert.equal(response.status, 403)
})
