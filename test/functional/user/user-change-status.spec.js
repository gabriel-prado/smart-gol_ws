'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - PUT /user/id/change-status')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let userApp = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  userApp = await Factory.model('App/Models/User').create({
    role: 'USER',
    status: false
  })
  userApp = userApp.toJSON()
})

test('should change user status to true and retrieve 200', async ({ client, assert }) => {
  const response = await client
    .put(`user/${userApp.id}/change-status`)
    .loginVia(backoffice)
    .send({
      status: true
    })
    .end()

  assert.equal(response.status, 200)
  assert.isNotNull(response.body)
  assert.equal(response.body.status, true)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = User.columns
  columns.splice(columns.indexOf('password'), 1)
  assert.containsAllKeys(response.body, columns)
})

test('should change user status to false and retrieve 200', async ({ client, assert }) => {
  const response = await client
    .put(`user/${userApp.id}/change-status`)
    .loginVia(backoffice)
    .send({
      status: false,
      denied_reason: "message"
    })
    .end()

  assert.equal(response.status, 200)
  assert.isNotNull(response.body)
  assert.equal(response.body.status, false)
  assert.equal(response.body.denied_reason, "message")
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = User.columns
  columns.splice(columns.indexOf('password'), 1)
  assert.containsAllKeys(response.body, columns)
})

test('should change user status to false and retrieve 400', async ({ client, assert }) => {
  const response = await client
    .put(`user/${userApp.id}/change-status`)
    .loginVia(backoffice)
    .send({
      status: false
    })
    .end()

  assert.equal(response.status, 400)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.put(`user/${userApp.id}/change-status`).loginVia(anotherUser).end()

  assert.equal(response.status, 403)
})

test('should update user without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.put(`user/${userApp.id}/change-status`).send(userApp).end()

  assert.equal(response.status, 401)
})
