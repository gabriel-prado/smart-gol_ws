'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - POST /user')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
})

test('should store user and retrieve 201', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').make()
  const userJson = user.toJSON()
  userJson.password = 'somePassword123'

  const response = await client.post('user').loginVia(backoffice).send(userJson).end()

  assert.equal(response.status, 201)
  const userInDatabase = await User.find(response.body.id)
  assert.isNotNull(userInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = User.columns
  columns.splice(columns.indexOf('password'), 1)
  assert.containsAllKeys(response.body, columns)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })

  const response = await client.post('user').loginVia(userApp).end()
  assert.equal(response.status, 403)
})

test('should store invalid user and retrieve 400', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').make()
  const userJson = user.toJSON()
  userJson.password = 'somePassword123'
  delete userJson.email;

  const response = await client.post('user').loginVia(backoffice).send(userJson).end()

  assert.equal(response.status, 400)
})

test('should store user without authenticate and retrieve 401', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').make()
  const userJson = user.toJSON()

  const response = await client.post('user').send(userJson).end()

  assert.equal(response.status, 401)
})
