'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - PUT /user')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let userApp = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })
})

test('should update user and retrieve 200 by backoffice', async ({ client, assert }) => {
  const userJson = backoffice.toJSON()
  userJson.password = 'somePassword123'
  userJson.username = 'new_name'

  const response = await client.put(`user/${userJson.id}`).loginVia(backoffice).send(userJson).end()

  assert.equal(response.status, 200)
  const userInDatabase = await User.find(response.body.id)
  assert.isNotNull(userInDatabase)
  assert.equal(userInDatabase.username, 'new_name')
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = User.columns
  columns.splice(columns.indexOf('password'), 1)
  assert.containsAllKeys(response.body, columns)
})

test('should update user and retrieve 200 by userApp', async ({ client, assert }) => {
  const userJson = userApp.toJSON()
  userJson.password = 'somePassword123'
  userJson.username = 'new_name'

  const response = await client.put(`user/${userJson.id}`).loginVia(userApp).send(userJson).end()

  assert.equal(response.status, 200)
  const userInDatabase = await User.find(response.body.id)
  assert.isNotNull(userInDatabase)
  assert.equal(userInDatabase.username, 'new_name')
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = User.columns
  columns.splice(columns.indexOf('password'), 1)
  assert.containsAllKeys(response.body, columns)
})

test('should update invalid user and retrieve 400', async ({ client, assert }) => {
  const userJson = backoffice.toJSON()
  userJson.password = 'somePassword123'
  userJson.email = 'email_invalid'

  const response = await client.put(`user/${userJson.id}`).loginVia(backoffice).send(userJson).end()

  assert.equal(response.status, 400)
})

test('should update user without authenticate and retrieve 401', async ({ client, assert }) => {
  const userJson = backoffice.toJSON()

  const response = await client.put(`user/${userJson.id}`).send(userJson).end()

  assert.equal(response.status, 401)
})
