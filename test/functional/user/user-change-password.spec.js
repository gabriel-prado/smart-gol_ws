'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - PUT /user/change-password')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let userApp = null

beforeEach(async () => {
  await clearDataBase()
  userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })
})

test('should change user password and retrieve 204', async ({ client, assert }) => {
  const response = await client
    .put(`user/change-password`)
    .send({email: userApp.email})
    .end()
    
  assert.equal(response.status, 204)
  const userInDatabase = await User.find(userApp.id)
  assert.notEqual(userInDatabase.updated_at, userApp.updated_at)
})

test('should change password without email and retrieve 400', async ({ client, assert }) => {
  const response = await client
    .put(`user/change-password`)
    .send()
    .end()

  assert.equal(response.status, 400)
})
