'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - GET /user/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
})

test('should retrieve existent user', async({ client, assert}) => {
  const response = await client.get(`user/${backoffice.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 200)
  let columns = User.columns
  columns.splice(columns.indexOf('password'), 1)
  assert.containsAllKeys(response.body, columns)
  assert.equal(response.body.id, backoffice.id)
})

test('should get 404 for a non existent user', async({ client, assert}) => {
  const response = await client.get(`user/10`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should not get user for not logged user', async({ client, assert}) => {
  const response = await client.get(`user/${backoffice.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.get('user/1').loginVia(userApp).end()

  assert.equal(response.status, 403)
})
