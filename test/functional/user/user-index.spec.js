'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - GET /user')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  await Factory.model('App/Models/User').createMany(5)
})

test('should list users without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.get('user').end()

  assert.equal(response.status, 401)
})

test('should list all users by name', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').createMany(2, {
    username: 'name test to search'
  })
  const response = await client.get('user').query({ name: 'name test'}).loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 2)
  assert.equal(response.body[0].name, user[0].name)
  assert.equal(response.body[1].name, user[1].name)
})

test('should list all users', async ({ client, assert }) => {
  const response = await client.get('user').loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 6)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.get('user').loginVia(anotherUser).end()

  assert.equal(response.status, 403)
})
