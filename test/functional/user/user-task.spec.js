'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const Company = use('App/Models/Company')
const Question = use('App/Models/Question')
const TaskQuestion = use('App/Models/TaskQuestion')
const { test, trait, beforeEach } = use('Test/Suite')('User - GET /user/id/task')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let userApp = null
let tasks = null
let questions = null

beforeEach(async () => {
  await clearDataBase()
  userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })
  tasks = await Factory.model('App/Models/Task').createMany(2, {
    user_id: userApp.id
  })

  questions = await Factory.model('App/Models/Question').createMany(2, {
    question_group: 'biscoito'
  })

  tasks.forEach(t => {
    questions.forEach(async q => {
      await Factory.model('App/Models/TaskQuestion').create({
        question_id: q.id,
        task_id: t.id
      })
    })
  })
})

test('should list tasks of user without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.get(`user/${userApp.id}/task`).end()

  assert.equal(response.status, 401)
})

test('should list tasks of user logged and retrieve 200', async ({ client, assert }) => {
  const response = await client.get(`user/${userApp.id}/task`).loginVia(userApp).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 2)
  assert.containsAllKeys(response.body[0].company, Company.columns)
  assert.containsAllKeys(response.body[0].questions, ['biscoito'])
  assert.lengthOf(response.body[0].questions['biscoito'], 2)
  assert.containsAllKeys(response.body[0].questions['biscoito'][0], Question.columns)
  assert.containsAllKeys(response.body[0].questions['biscoito'][0].answer, TaskQuestion.columns)
})

test('should try list tasks of another user and retrieve 403', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.get(`user/${anotherUser.id}/task`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
