'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - POST /sign-up')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should store user by app and retrieve 201', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').make()
  const userJson = user.toJSON()
  userJson.password = 'somePassword123'

  const response = await client.post('sign-up').send(userJson).end()

  assert.equal(response.status, 201)
  const userInDatabase = await User.find(response.body.id)
  assert.isNotNull(userInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = User.columns
  columns.splice(columns.indexOf('password'), 1)
  assert.containsAllKeys(response.body, columns)
  assert.equal(userInDatabase.role, 'USER')
})

test('should store invalid user and retrieve 400', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').make()
  const userJson = user.toJSON()
  userJson.password = 'somePassword123'
  delete userJson.email;

  const response = await client.post('sign-up').send(user).end()

  assert.equal(response.status, 400)
})
