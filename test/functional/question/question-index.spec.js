'use strict'

const Factory = use('Factory')
const Question = use('App/Models/Question')
const { test, trait, beforeEach } = use('Test/Suite')('Question - GET /question')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  await Factory.model('App/Models/Question').createMany(5)
})

test('should list questions without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.get('question').end()

  assert.equal(response.status, 401)
})

test('should list all questions', async ({ client, assert }) => {
  const response = await client.get('question').loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 5)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.get('question').loginVia(userApp).end()

  assert.equal(response.status, 403)
})
