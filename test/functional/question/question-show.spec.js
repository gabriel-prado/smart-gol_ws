'use strict'

const Factory = use('Factory')
const Question = use('App/Models/Question')
const { test, trait, beforeEach } = use('Test/Suite')('Question - GET /question/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let question = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  question = await Factory.model('App/Models/Question').create()
})

test('should retrieve existent question', async({ client, assert}) => {
  const response = await client.get(`question/${question.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, Question.columns)
  assert.equal(response.body.id, question.id)
})

test('should get 404 for a non existent question', async({ client, assert}) => {
  const response = await client.get(`question/10`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should not get question for not logged user', async({ client, assert}) => {
  const response = await client.get(`question/${question.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.get('question/1').loginVia(userApp).end()

  assert.equal(response.status, 403)
})
