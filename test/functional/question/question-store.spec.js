'use strict'

const Factory = use('Factory')
const Question = use('App/Models/Question')
const { test, trait, beforeEach } = use('Test/Suite')('Question - POST /question')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let question = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  question = await Factory.model('App/Models/Question').make()
})

test('should store question and retrieve 201', async ({ client, assert }) => {
  const response = await client.post('question').loginVia(backoffice).send(question.toJSON()).end()

  assert.equal(response.status, 201)
  const questionInDatabase = await Question.find(response.body.id)
  assert.isNotNull(questionInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Question.columns)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })

  const response = await client.post('question').loginVia(userApp).end()
  assert.equal(response.status, 403)
})

test('should store invalid question and retrieve 400', async ({ client, assert }) => {
  const questionJson = question.toJSON()
  delete questionJson.type;

  const response = await client.post('question').loginVia(backoffice).send(questionJson).end()

  assert.equal(response.status, 400)
})

test('should store question without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.post('question').send(question.toJSON()).end()

  assert.equal(response.status, 401)
})
