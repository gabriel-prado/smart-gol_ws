'use strict'

const Factory = use('Factory')
const Question = use('App/Models/Question')
const { test, trait, beforeEach } = use('Test/Suite')('Question - PUT /question')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let question = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  question = await Factory.model('App/Models/Question').create()
})

test('should update question and retrieve 200', async ({ client, assert }) => {
  const questionJson = question.toJSON()
  questionJson.question_title = 'new_question_title'

  const response = await client.put(`question/${question.id}`).loginVia(backoffice).send(questionJson).end()

  assert.equal(response.status, 200)
  const questionInDatabase = await Question.find(response.body.id)
  assert.isNotNull(questionInDatabase)
  assert.equal(questionInDatabase.question_title, 'new_question_title')
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Question.columns)
})

test('should update invalid question and retrieve 400', async ({ client, assert }) => {
  const questionJson = question.toJSON()
  questionJson.type = 'wrong_type'

  const response = await client.put(`question/${question.id}`).loginVia(backoffice).send(questionJson).end()

  assert.equal(response.status, 400)
})

test('should update question without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.put(`question/${question.id}`).send(question.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.put(`question/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
