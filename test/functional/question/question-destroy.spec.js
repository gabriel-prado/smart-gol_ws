'use strict'

const Factory = use('Factory')
const Question = use('App/Models/Question')
const { test, trait, beforeEach } = use('Test/Suite')('Question - DELETE /question/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
})

test('should destroy question by id', async ({ client, assert }) => {
  const question = await Factory.model('App/Models/Question').create()
  const response = await client.delete(`question/${question.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 204)
  const questionInDatabase =  await Question.find(question.id)
  assert.isNull(questionInDatabase)
})

test('should try to destroy non-existent question and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`question/123`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should try destroy question without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`question/12`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.delete(`question/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
