'use strict'

const Factory = use('Factory')
const Task = use('App/Models/Task')
const { test, trait, beforeEach } = use('Test/Suite')('Task - POST /task')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let task = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  task = await Factory.model('App/Models/Task').make()
})

test('should store task and retrieve 201', async ({ client, assert }) => {
  const response = await client.post('task').loginVia(backoffice).send(task.toJSON()).end()

  assert.equal(response.status, 201)
  const taskInDatabase = await Task.find(response.body.id)
  assert.isNotNull(taskInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Task.columns)
})

test('should try to access the route without the required role and retrieve 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })

  const response = await client.post('task').loginVia(userApp).end()
  assert.equal(response.status, 403)
})

test('should store invalid task and retrieve 400', async ({ client, assert }) => {
  const taskJson = task.toJSON()
  delete taskJson.points;

  const response = await client.post('task').loginVia(backoffice).send(taskJson).end()

  assert.equal(response.status, 400)
})

test('should store task without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.post('task').send(task.toJSON()).end()

  assert.equal(response.status, 401)
})
