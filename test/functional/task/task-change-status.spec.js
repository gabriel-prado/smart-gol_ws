'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const Task = use('App/Models/Task')
const { test, trait, beforeEach } = use('Test/Suite')('Task - PUT /task/id/change-status')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let userApp = null
let task = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  userApp = await Factory.model('App/Models/User').create({
    role: 'USER'
  })
  userApp = userApp.toJSON()
  task = await Factory.model('App/Models/Task').create({
    approved: null
  })
})

test('should change task status to true and retrieve 200', async ({ client, assert }) => {
  const response = await client
    .put(`task/${task.id}/change-status`)
    .loginVia(backoffice)
    .send({
      approved: true
    })
    .end()

  assert.equal(response.status, 200)
  assert.isNotNull(response.body)
  assert.equal(response.body.approved, true)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = Task.columns
  assert.containsAllKeys(response.body, columns)
})

test('should change task status to false and retrieve 200', async ({ client, assert }) => {
  const response = await client
    .put(`task/${task.id}/change-status`)
    .loginVia(backoffice)
    .send({
      approved: false
    })
    .end()

  assert.equal(response.status, 200)
  assert.isNotNull(response.body)
  assert.equal(response.body.approved, false)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  let columns = Task.columns
  assert.containsAllKeys(response.body, columns)
})

test('should change task without status and retrieve 400', async ({ client, assert }) => {
  const response = await client
    .put(`task/${task.id}/change-status`)
    .loginVia(backoffice)
    .end()

  assert.equal(response.status, 400)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.put(`task/${task.id}/change-status`).loginVia(anotherUser).end()

  assert.equal(response.status, 403)
})

test('should update user without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.put(`task/${task.id}/change-status`).send(userApp).end()

  assert.equal(response.status, 401)
})
