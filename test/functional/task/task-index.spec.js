'use strict'

const Factory = use('Factory')
const Task = use('App/Models/Task')
const { test, trait, beforeEach } = use('Test/Suite')('Task - GET /task')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  await Factory.model('App/Models/Task').createMany(5)
})

test('should list tasks without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.get('task').end()

  assert.equal(response.status, 401)
})

test('should list all tasks', async ({ client, assert }) => {
  const response = await client.get('task').loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body[0], ['company', 'user'])
  assert.lengthOf(response.body, 5)
})
