'use strict'

const Factory = use('Factory')
const Task = use('App/Models/Task')
const { test, trait, beforeEach } = use('Test/Suite')('Task - PUT /task')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let task = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  task = await Factory.model('App/Models/Task').create()
})

test('should update task and retrieve 200', async ({ client, assert }) => {
  const taskJson = task.toJSON()
  taskJson.description = 'new_description'

  const response = await client.put(`task/${task.id}`).loginVia(backoffice).send(taskJson).end()

  assert.equal(response.status, 200)
  const taskInDatabase = await Task.find(response.body.id)
  assert.isNotNull(taskInDatabase)
  assert.equal(taskInDatabase.description, 'new_description')
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body, Task.columns)
})

test('should update invalid task and retrieve 400', async ({ client, assert }) => {
  const taskJson = task.toJSON()
  taskJson.points_bonus = 'wrong points'

  const response = await client.put(`task/${task.id}`).loginVia(backoffice).send(taskJson).end()

  assert.equal(response.status, 400)
})

test('should update task without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.put(`task/${task.id}`).send(task.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.put(`task/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
