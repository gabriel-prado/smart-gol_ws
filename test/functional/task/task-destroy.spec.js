'use strict'

const Factory = use('Factory')
const Task = use('App/Models/Task')
const { test, trait, beforeEach } = use('Test/Suite')('Task - DELETE /task/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
})

test('should destroy task by id', async ({ client, assert }) => {
  const task = await Factory.model('App/Models/Task').create()
  const response = await client.delete(`task/${task.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 204)
  const taskInDatabase =  await Task.find(task.id)
  assert.isNull(taskInDatabase)
})

test('should try to destroy non-existent task and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`task/123`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should try destroy task without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`task/12`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 403', async ({ client, assert }) => {
  const userApp = await Factory.model('App/Models/User').create({ role: 'USER' })
  const response = await client.delete(`task/1`).loginVia(userApp).end()

  assert.equal(response.status, 403)
})
