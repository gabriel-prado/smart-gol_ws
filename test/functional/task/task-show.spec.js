'use strict'

const Factory = use('Factory')
const Task = use('App/Models/Task')
const { test, trait, beforeEach } = use('Test/Suite')('Task - GET /task/id')
const clearDataBase = require('../../utils/clearDataBase')

trait('Test/ApiClient')
trait('Auth/Client')

let backoffice = null
let task = null

beforeEach(async () => {
  await clearDataBase()
  backoffice = await Factory.model('App/Models/User').create({
    role: 'BACKOFFICE',
    permissions: 'ALL'
  })
  task = await Factory.model('App/Models/Task').create()
})

test('should retrieve existent task', async({ client, assert}) => {
  const response = await client.get(`task/${task.id}`).loginVia(backoffice).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, Task.columns)
  assert.containsAllKeys(response.body, ['questions', 'company', 'client', 'user'])
  assert.equal(response.body.id, task.id)
})

test('should get 404 for a non existent task', async({ client, assert}) => {
  const response = await client.get(`task/10`).loginVia(backoffice).end()

  assert.equal(response.status, 404)
})

test('should not get task for not logged user', async({ client, assert}) => {
  const response = await client.get(`task/${task.id}`).end()

  assert.equal(response.status, 401)
})
