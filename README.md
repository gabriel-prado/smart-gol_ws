# Start

Copy and rename the .env.example file to .env

```bash
npm install
adonis serve --dev
```

## Swagger

You can access the swagger through this [link](http://localhost:3333/docs).

To access most routes, it is necessary to run the login-app or login-portal endpoint and copy the token (without the Bearer) and insert it into the Authorize button or individually on each route.


```bash
http://localhost:3333/docs
```

## Migrations

```js
adonis migration:run
```

## Seed

```js
adonis seed
```

## Functional Tests

```js
adonis test
```

## Author
Gabriel Prado

gabriel.prado.profeta@gmail.com
