'use strict'

const ace = require('@adonisjs/ace')
const clearDataBase = require('./test/utils/clearDataBase')

module.exports = (cli, runner) => {
  runner.timeout(20000)

  runner.before(async () => {
    use('Adonis/Src/Server').listen(process.env.HOST, process.env.PORT)
    await ace.call('migration:run', {}, { silent: true })
  })

  runner.after(async () => {
    use('Adonis/Src/Server').getInstance().close()
    await clearDataBase()
    await ace.call('migration:reset', {}, { silent: true })
  })
}
